# import invest_core as ic
# from flask import Flask
from flask_sqlalchemy import SQLAlchemy
# from flask_migrate import Migrate
from flask_login import LoginManager
# from flask_mail import Mail

from flask_bootstrap import Bootstrap
from flask_moment import Moment
# from redis import Redis
# import rq
from . import terminal

# ic.config.configure_from_json(r'C:\invest_robot\config\config_win_test.json')
# ic.config.choose_server('local')

bootstrap = Bootstrap()
moment = Moment()
login = LoginManager()
db = SQLAlchemy()
# migrate = Migrate()
# mail = Mail()

app = terminal.create_app(db=db, bootstrap=bootstrap,  # migrate=migrate,
                          moment=moment, login=login, terminal_config={'target_db': 'local', 'secret_key': 'wtf'})
