import os
# from dotenv import load_dotenv
import logging
from logging.handlers import RotatingFileHandler
# from .config import flask_config
# import invest_core as ic
from flask import Flask
# import invest_core as ic
# from invest_robot.robot import RedisConfig, RedisInterface, Logger
import datetime as dt


'''
basedir = os.path.abspath(os.path.dirname(__file__))
# load_dotenv(os.path.join(basedir, '.env'))


class Config(object):
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'you-will-never-guess'
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL') or \
        'sqlite:///' + os.path.join(basedir, '../app.db')
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    LOG_TO_STDOUT = os.environ.get('LOG_TO_STDOUT')
    MAIL_SERVER = os.environ.get('MAIL_SERVER')
    MAIL_PORT = int(os.environ.get('MAIL_PORT') or 25)
    MAIL_USE_TLS = os.environ.get('MAIL_USE_TLS') is not None
    MAIL_USERNAME = os.environ.get('MAIL_USERNAME')
    MAIL_PASSWORD = os.environ.get('MAIL_PASSWORD')
    ADMINS = ['m.s.pogodin@gmail.com']
    REDIS_URL = os.environ.get('REDIS_URL') or 'redis://'
    PER_PAGE = 25


flask_config = Config()
'''


def create_app(db, bootstrap, moment, login, terminal_config: dict = {}):  # migrate,
    app = Flask(__name__)
    # app.module_name = 'terminal'
    # app.ts_init = dt.datetime.utcnow()
    # app.date = dt.datetime.utcnow().date()

    # app.rkey_terminal = f"{app.date:%Y-%m-%d}:control:terminal"
    # redis_config = RedisConfig(is_main=True)
    # app.log = Logger(app.rkey_terminal, redis_config)
    # app.logger = app.log  # !@!*+?
    # app.redis = RedisInterface(redis_config, app.logger)
    # app.log.info(f"{app.ts_init.isoformat()}", "init")
    # app.redis.hmset(app.rkey_terminal, "config", {'ts_init': app.ts_init.isoformat()})

    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
    # app.config['SECRET_KEY'] = terminal_config['secret_key']
    app.config['SECRET_KEY'] = 'secret_key'
    # app.config['SQLALCHEMY_DATABASE_URI'] = ic.config.db('connection_string', terminal_config.get('target_db', 'main'))
    app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///app.db'
    # app.config.from_object(terminal_config)
    # bootstrap.init_app(app)
    moment.init_app(app)
    db.init_app(app)
    db.app = app
    # migrate.init_app(app, db)
    # mail.init_app(application)

    # login.login_view = terminal_config.get('login_view', 'auth.login')
    # login.login_message = terminal_config.get('login_message', 'SignIn')
    # login.init_app(app)
    # application.task_queue = rq.Queue('tasks', connection=application.redis)

    from .api import bp as api_bp
    app.register_blueprint(api_bp, url_prefix='/api')

    from .main import bp as main_bp
    app.register_blueprint(main_bp, url_prefix='/')

    # app.logger.info('init')
    # if not os.path.exists('logs'):
    #     os.mkdir('logs')
    # file_handler = RotatingFileHandler('logs/app.log',
    #                                    maxBytes=10240, backupCount=10)
    # file_handler.setFormatter(logging.Formatter(
    #     '%(asctime)s %(levelname)s: %(message)s '
    #     '[in %(pathname)s:%(lineno)d]'))
    # file_handler.setLevel(logging.INFO)
    # app.logger.addHandler(file_handler)

    app.logger.setLevel(logging.INFO)
    app.logger.info('App startup')
    return app
