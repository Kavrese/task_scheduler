from datetime import datetime as dt
from sqlalchemy import create_engine
from flask import request, jsonify
import time
from functools import wraps
from .api.errors import *

HOST_DB = "46.138.242.199"
PORT_DP = "5432"
NAME_DB = "idc"
USER_DB = "lyubimov"
PASSWORD_DB = "Aa!135"


def connect_to_db():
    conn = create_engine(f'postgresql+psycopg2://{USER_DB}:{PASSWORD_DB}@{HOST_DB}:{PORT_DP}/{NAME_DB}').connect()
    return conn


# Проверка параметров (старая)
def check_params(params, request_):
    for i in params:
        if request_.args.get(i, type=str) is None:
            return error_response(404, message=f"{i} not found in params request")
    return None


def get_system_datetime():
    return dt.now().strftime("%a, %d %b %Y %H:%M:%S.000")


def convert_dict_to_str(dict_):
    str_dict = str(dict_).replace("'", "\"")
    return str_dict


def convert_results_to_dict(result):
    return [i.to_dict() for _, i in result.iterrows()][0] if not result.empty else {}


def convert_clear_user_results_to_dict(result, delete_keys=("password",)):
    user = [i.to_dict() for _, i in result.iterrows()][0]
    for key in delete_keys:
        del user[key]
    return user


def convert_results_to_list_dict(result):
    return [i.to_dict() for _, i in result.iterrows()] if not result.empty else []


# Проверка параметров в запросе
# params - словарь (параметр: "param" или "body") обязательных параметров
def required_params(params: dict):
    def inner(func):
        def wrapper(*args, **kwargs):

            check_body = not all([params[i] == "param" for i in params])
            check_param = not all([params[i] == "body" for i in params])

            if check_body and request.get_json() is None:
                return error_response(400, f'body not found in request')

            if check_param:
                need_params = [i for i in params if params[i] == "param"]
                no_exist_params = [param for param in need_params if param not in request.args]
                if len(no_exist_params):
                    return error_response(400,
                                          f"{', '.join(no_exist_params)} not found in request params")
            if check_body:
                need_body_params = [i for i in params if params[i] == "body"]
                no_exist_body_params = [param for param in need_body_params if param not in request.get_json()]
                if len(no_exist_body_params):
                    return error_response(400,
                                          f"{', '.join(no_exist_body_params)} not found in request body params")

            return func(*args, **kwargs)

        wrapper.__name__ = func.__name__
        return wrapper

    return inner


def convert_status_id_to_str(status: int) -> str:
    return {-3: "Пауза",
            -2: "Просрочена",
            -1: "Отменена",
            0: "Выдана",
            1: "В работе",
            2: "Ожидает подтверждения",
            3: "Выполнена"}[status]


def convert_string_to_datetime(date: str):
    return dt.strptime(date, "%a, $d %b %Y %H:%M:S").timestamp()


def mult_threading(func):
    """Декоратор для запуска функции в отдельном потоке"""
    @wraps(func)
    def wrapper(*args_, **kwargs_):
        import threading
        func_thread = threading.Thread(target=func,
                                       args=tuple(args_),
                                       kwargs=kwargs_)
        func_thread.start()
        return func_thread

    return wrapper
