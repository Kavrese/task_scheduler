from flask import render_template, flash, redirect, url_for, request, \
    jsonify, current_app, make_response

from . import bp
from .forms import LoginForm


@bp.route('/', methods=['GET', 'POST'])
@bp.route('/index', methods=['GET', 'POST'])
def index():
    error = ''
    if request.method == 'GET':
        return render_template('index.html', error=error)
    else:
        form = request.form
        email = form.get('email')
        password = form.get('password')

        if email != 'admin@idc.ru':
            error = "Неправильно введен email"
            return render_template('index.html', error=error)
        elif password != "134679idc":
            error = "Неправильно введен пароль"
            return render_template('index.html', error=error)
        else:
            return redirect(url_for('main.main_panel'))


@bp.route('/main_panel')
def main_panel():
    return render_template('main_panel.html')
