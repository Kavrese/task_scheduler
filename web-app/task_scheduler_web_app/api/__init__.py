from flask import Blueprint
from pyfcm import FCMNotification
from ..utils import connect_to_db
from .send_notification import SenderNotification
from .SchedulerNotification import SchedulerNotification
from .request_to_db import RequestDB

bp = Blueprint('api', __name__)
request_db = RequestDB(connect_to_db())
push_service = FCMNotification(
    api_key="AAAA8Emu4yc:APA91bHr17LxPPNCxAQ7uFdFiMQ3WCPgTCq7aQzjAz16KgghtxiM6SAexD8ns1wKyv62RZCmsdDEKhU3AzqFfVcVCln6hpAJE0s8ubQgr8hci5EVy5YiM3-4yGhFNBWW-PInijFlod3I"
)
sender_notification = SenderNotification(push_service, request_db)
scheduler_notification = SchedulerNotification(sender_notification)
scheduler_notification.init_background_notifications()

from . import api
