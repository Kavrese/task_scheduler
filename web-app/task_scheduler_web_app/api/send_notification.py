from .request_to_db import RequestDB
from ..utils import *


class SenderNotification:
    def __init__(self, push_service, request_db: RequestDB):
        self.push_service = push_service
        self.request_db = request_db

    def send_notification_persons_task_change_status(self, id_task: int, new_status: int, reg_ids: list):
        task = self.request_db.get_task(id_task)
        author = convert_results_to_dict(self.request_db.select_user_from_id(task["person_id"]))
        title = f"Изменение статуса задачи {author['secondname']} {author['firstname'][0]}. {author['lastname'][0]}."
        message = f"Задача, над которой вы работаете, изменила статус на {convert_status_id_to_str(new_status)}. Нажмите что бы посмотреть."
        return self.send_notification_connect_to_task(id_task, reg_ids, title, message)

    def send_notification_persons_new_task(self, id_task: int, reg_ids: list):
        task = self.request_db.get_task(id_task)
        author = convert_results_to_dict(self.request_db.select_user_from_id(task["person_id"]))
        title = f"Новая задача от {author['secondname']} {author['firstname'][0]}. {author['lastname'][0]}."
        message = f"Нажмите что бы посмотреть."
        return self.send_notification_connect_to_task(id_task, reg_ids, title, message)

    def send_notification_add_task(self, id_task: int, reg_ids: list):
        task = self.request_db.get_task(id_task)
        author = convert_results_to_dict(self.request_db.select_user_from_id(task["person_id"]))
        title = f"Вас добавили в задачу от {author['secondname']} {author['firstname'][0]}. {author['lastname'][0]}."
        message = f"Нажмите что бы посмотреть."
        return self.send_notification_connect_to_task(id_task, reg_ids, title, message)

    def send_notification_complete_task(self, id_task: int, reg_ids: list):
        task = self.request_db.get_task(id_task)
        author = convert_results_to_dict(self.request_db.select_user_from_id(task["person_id"]))
        title = f"Задача от {author['secondname']} {author['firstname'][0]}. {author['lastname'][0]} выполнена."
        message = f"Нажмите что бы посмотреть."
        return self.send_notification_connect_to_task(id_task, reg_ids, title, message)

    def send_notification_cancel_task(self, id_task: int, reg_ids: list):
        task = self.request_db.get_task(id_task)
        author = convert_results_to_dict(self.request_db.select_user_from_id(task["person_id"]))
        title = f"Задача от {author['secondname']} {author['firstname'][0]}. {author['lastname'][0]} отменена."
        message = f"Нажмите что бы посмотреть."
        return self.send_notification_connect_to_task(id_task, reg_ids, title, message)

    def send_notification_need_complete_task(self, id_task: int, reg_ids: list):
        task = self.request_db.get_task(id_task)
        author = convert_results_to_dict(self.request_db.select_user_from_id(task["person_id"]))
        title = f"Задача от {author['secondname']} {author['firstname'][0]}. {author['lastname'][0]} подана на проверку."
        message = f"Нажмите что бы посмотреть."
        return self.send_notification_connect_to_task(id_task, reg_ids, title, message)

    def send_notification_change_task(self, id_task: int, reg_ids: list):
        task = self.request_db.get_task(id_task)
        author = convert_results_to_dict(self.request_db.select_user_from_id(task["person_id"]))
        title = f"Появились изменения в задаче от {author['secondname']} {author['firstname'][0]}. {author['lastname'][0]}."
        message = f"Нажмите что бы посмотреть."
        return self.send_notification_connect_to_task(id_task, reg_ids, title, message)

    def send_notification_deadline_soon_task(self, id_task: int, reg_ids: list):
        task = self.request_db.get_task(id_task)
        author = convert_results_to_dict(self.request_db.select_user_from_id(task["person_id"]))
        title = f"Срок задачи от {author['secondname']} {author['firstname'][0]}. {author['lastname'][0]} близится к концу."
        message = f"Срок задачи \"{task['title']}\" скоро завершится. Нажмите что бы посмотреть задачу."
        return self.send_notification_connect_to_task(id_task, reg_ids, title, message)

    def send_notification_new_deadline_task(self, id_task: int, reg_ids: list):
        task = self.request_db.get_task(id_task)
        author = convert_results_to_dict(self.request_db.select_user_from_id(task["person_id"]))
        title = f"Срок задачи от {author['secondname']} {author['firstname'][0]}. {author['lastname'][0]} изменился."
        message = f"Новый срок задачи \"{task['title']}\". Нажмите что бы посмотреть задачу."
        return self.send_notification_connect_to_task(id_task, reg_ids, title, message)

    def send_notification_now_no_work(self, id_task: int, reg_ids: list):
        task = self.request_db.get_task(id_task)
        author = convert_results_to_dict(self.request_db.select_user_from_id(task["person_id"]))
        title = f"Вы теперь не участвуете в задаче от {author['secondname']} {author['firstname'][0]}. {author['lastname'][0]}."
        message = f"Теперь вы не можете просмотреть эту задачу."
        return self.send_multiple_devices(reg_ids, title, message, {})

    def send_notification_now_boss(self, id_task: int, reg_ids: list):
        task = self.request_db.get_task(id_task)
        author = convert_results_to_dict(self.request_db.select_user_from_id(task["person_id"]))
        title = f"Вы теперь руководитель в задаче от {author['secondname']} {author['firstname'][0]}. {author['lastname'][0]}."
        message = f"Нажмите что бы посмотреть задачу."
        return self.send_notification_connect_to_task(id_task, reg_ids, title, message)

    def send_notification_now_executor(self, id_task: int, reg_ids: list):
        task = self.request_db.get_task(id_task)
        author = convert_results_to_dict(self.request_db.select_user_from_id(task["person_id"]))
        title = f"Вы теперь исполнитель в задаче от {author['secondname']} {author['firstname'][0]}. {author['lastname'][0]}."
        message = f"Нажмите что бы посмотреть задачу."
        return self.send_notification_connect_to_task(id_task, reg_ids, title, message)

    def send_notification_8_00(self, reg_ids: list, person_id: int):
        person = convert_results_to_dict(self.request_db.select_user_from_id(person_id))
        count_person_tasks = self.request_db.count_person_task(person_id)
        count_executor = self.request_db.count_task_where_executor(person_id)
        count_boss = self.request_db.count_task_where_boss(person_id)

        message = f"У вас всего {count_person_tasks} задач, из который {count_executor} вы указаны как исполнитель."
        title = f"Доброе утро и удачного дня"

        data_message = {
            "type": "every_day"
        }

        return self.send_multiple_devices(reg_ids, title, message, data_message)

    def send_notification_17_30(self, reg_ids: list, person_id: int):
        person = convert_results_to_dict(self.request_db.select_user_from_id(person_id))
        count_person_tasks = self.request_db.count_person_task(person_id)
        count_executor = self.request_db.count_task_where_executor(person_id)
        count_boss = self.request_db.count_task_where_boss(person_id)

        message = f"У вас осталось всего {count_person_tasks} задач, из который {count_executor} вы указаны как исполнитель."
        title = f"Доброго вечера."

        data_message = {
            "type": "every_day"
        }

        return self.send_multiple_devices(reg_ids, title, message, data_message)

    def send_statistic_notifications(self, is_morning: bool):
        all_person = convert_results_to_list_dict(self.request_db.select_all_person())
        for person in all_person:
            id_person = person["person_id"]
            ids = [i["registration_id"] for i in
                   convert_results_to_list_dict(self.request_db.select_person_registration_ids(id_person))]
            if len(ids) != 0:
                if is_morning:
                    self.send_notification_8_00(ids, id_person)
                else:
                    self.send_notification_17_30(ids, id_person)

    def send_notification_connect_to_task(self, id_task: int, reg_ids: list, title: str, message: str):
        data_message = {
            "type": "for_task",
            "id_task": f"{id_task}"
        }
        return self.send_multiple_devices(reg_ids, title, message, data_message)

    def send_notification_to_all_persons_task(self, id_task: int, message_title: str, message_body: str, data: dict):
        reg_ids = self.request_db.get_all_registrations_ids_from_task(id_task)
        return self.send_multiple_devices(reg_ids, message_title, message_body, data)

    def send_multiple_devices(self, reg_ids: list, message_title: str, message_body: str, data: dict):
        try:
            result = self.push_service.notify_multiple_devices(registration_ids=reg_ids, message_title=message_title,
                                                               message_body=message_body, data_message=data)
            return result
        except:
            return None

    def send_single_device(self, reg_id: str, message_title: str, message_body: str, data: dict):
        try:
            result = self.push_service.notify_single_devices(registration_id=reg_id, message_title=message_title,
                                                             message_body=message_body, data_message=data)
            return result
        except:
            return None

    def send_notification_for_status(self, new_status: int, id_task: int):
        if new_status in [-3, -2, 0, 1]:
            self.send_notification_persons_task_change_status(id_task, new_status,
                                                              self.request_db.get_all_registrations_ids_from_task(
                                                                  id_task))
        elif new_status == -1:
            self.send_notification_cancel_task(id_task, self.request_db.get_all_registrations_ids_from_task(id_task))
        elif new_status == 2:
            self.send_notification_need_complete_task(id_task,
                                                      self.request_db.get_boss_registrations_ids_from_task(id_task))
        elif new_status == 3:
            self.send_notification_complete_task(id_task, self.request_db.get_all_registrations_ids_from_task(id_task))
