import pandas as pd
from ..utils import *


class RequestDB:
    def __init__(self, conn):
        self.conn = conn

    def get_task(self, id_task: str) -> dict:
        task = convert_results_to_dict(self.select_task_from_id(id_task))
        persons = convert_results_to_list_dict(self.select_persons_from_task(id_task))
        author_person = self.get_person_from_user(task["person_id"], 1)
        person_role_1 = list(filter(lambda x: x["role_id"] == 1, persons))
        person_role_2 = list(filter(lambda x: x["role_id"] == 2, persons))
        if author_person["person_id"] not in [id["person_id"] for id in person_role_1]:
            person_role_1.append(author_person)
        task["persons_role_1"] = person_role_1
        task["persons_role_2"] = person_role_2

        return task

    def task_is_exist(self, id_task: str) -> bool:
        result = pd.read_sql(f'select * from task_scheduler.task where task_id = \'{id_task}\';', self.conn)
        return len(convert_results_to_dict(result)) != 0

    def select_persons_from_task(self, id_task):
        result = pd.read_sql(f'select p.person_id, p.firstname, p.secondname, p.lastname, pt.role_id from task_scheduler.person as p'
                             f' inner join task_scheduler.person_task as pt on p.person_id = pt.person_id'
                             f' inner join task_scheduler.task as t on pt.task_id = t.task_id where t.task_id = {id_task}',
                             self.conn)
        return result

    def get_ids_person_role(self, task_id, role_id) -> list:
        return [str(i["person_id"]) for i in list(filter(lambda x: x["role_id"] == role_id, convert_results_to_list_dict(self.select_persons_from_task(task_id))))]

    def get_person_from_user(self, person_id, role_id: int):
        user = convert_results_to_dict(self.select_user_from_id(person_id))
        return {"firstname": user["firstname"], "secondname": user["secondname"], "lastname": user["lastname"], "person_id": person_id, "role_id": role_id}

    def select_person_task_role(self, person_id):
        result = pd.read_sql(f'select * from task_scheduler.person_task where person_id = \'{person_id}\';',
                             self.conn)
        return result

    def select_person_task_with_role(self, person_id, role_id):
        result = pd.read_sql(f'select * from task_scheduler.person_task where person_id = \'{person_id}\' and '
                                     f'role_id = \'{role_id}\';', self.conn)
        return result

    def get_user_id_from_token(self, token):
        user = convert_results_to_dict(self.select_user_from_token(token))
        return user["person_id"]

    def select_all_person(self):
        result = pd.read_sql(f'select * from task_scheduler.person;', self.conn)
        return result

    def update_status(self, new_status, id_task):
        with self.conn.begin() as tran:
            try:
                self.conn.execute(f'update task_scheduler.task set status = {new_status} where task_id = \'{id_task}\';')
            except:
                tran.rollback()
                raise

    def select_task_from_id(self, id_task):
        result = pd.read_sql(f'select * from task_scheduler.task where task_id = \'{id_task}\';', self.conn)
        return result

    def select_tasks_between_time(self, start_time, end_time):
        result = pd.read_sql(f"select * from task_scheduler.task where ts_end between \'{start_time}\' and \'{end_time}\'", self.conn)
        return [self.get_task(task["task_id"]) for task in convert_results_to_list_dict(result)]

    def select_person_tasks_between_time(self, id_person, start_time, end_time):
        result = pd.read_sql(
            f"select * from task_scheduler.task where person_id = \'{id_person}\' and ts_end between \'{start_time}\' and \'{end_time}\'",
            self.conn)
        return [self.get_task(task["task_id"]) for task in convert_results_to_list_dict(result)]

    def insert_user(self, token, phone, email, password, firstname, lastname, secondname, person_level):
        with self.conn.begin() as tran:
            try:
                self.conn.execute(
                    f'insert into task_scheduler.person ("token", phone, email, "password", firstname, lastname, secondname, "position", person_level) '
                    f'values (\'{token}\', \'{phone}\', \'{email}\' ,\'{password}\', \'{firstname}\', \'{lastname}\', \'{secondname}\', \'{1}\', {person_level});')
            except:
                tran.rollback()
                raise

    def select_user(self, email, password):
        result = pd.read_sql(
            f'select * from task_scheduler.person where "email"=\'{email}\' and "password"=\'{password}\';',
            self.conn)
        return result

    def insert_new_token(self, token, email, password):
        with self.conn.begin() as tran:
            try:
                self.conn.execute(f'update task_scheduler.person set "token"=\'{token}\' where "email"=\'{email}\' and '
                             f'"password"=\'{password}\';')
            except:
                tran.rollback()
                raise

    def search_user(self, text_search):
        result = pd.read_sql(
            f'select * from task_scheduler.person where "secondname" LIKE (\'{text_search}%%\')or "firstname" LIKE (\'{text_search}%%\') or "lastname" LIKE (\'{text_search}%%\');',
            self.conn)
        return result

    def search_task(self, text_search):
        result = pd.read_sql(f'select "task_id" from task_scheduler.task where "title" LIKE (\'{text_search}%%\');',
                             self.conn)
        ids_task = [i["task_id"] for i in convert_results_to_list_dict(result)]
        return [self.get_task(id_task) for id_task in ids_task]

    def select_search(self, filter_search, text_search):
        return self.search_task(text_search) if filter_search == "task" else self.search_user(text_search)

    def select_user_from_id(self, person_id):
        user = pd.read_sql(f'select * from task_scheduler.person where "person_id" = \'{person_id}\';', self.conn)
        return user

    def select_user_from_token(self, token):
        user = pd.read_sql(f'select * from task_scheduler.person where "token" = \'{token}\';', self.conn)
        return user

    def insert_event(self, idTask, id_person, ts_, details):
        with self.conn.begin() as tran:
            try:
                self.conn.execute(
                    f'insert into task_scheduler."event" (task_id,person_id,ts,details) values ({idTask},{id_person},\'{ts_}\',\'{convert_dict_to_str(details)}\');')
            except:
                tran.rollback()
                raise

    def insert_task(self, ts_bgn, ts_end, ts_init, person_id, title, args, status):
        with self.conn.begin() as tran:
            try:
                self.conn.execute(
                    f"insert into task_scheduler.task (ts_bgn,ts_end,ts_init,person_id,title,args,status) values ('{ts_bgn}','{ts_end}','{ts_init}',{person_id},'{title}','{convert_dict_to_str(args)}',{status}); ")
            except:
                tran.rollback()
                raise

    def update_task_column(self, column: str, value: str, task_id: int):
        with self.conn.begin() as tran:
            try:
                self.conn.execute(
                    f"update task_scheduler.task set \"{column}\" = \'{value}\' where task_id = {task_id};")
            except:
                tran.rollback()
                raise

    def insert_person_task(self, task_id, person_id, role_id=2):
        with self.conn.begin() as tran:
            try:
                self.conn.execute(
                    f'insert into task_scheduler.person_task (task_id,person_id,role_id) values ({int(task_id)},{int(person_id)},{int(role_id)});')
            except:
                tran.rollback()
                raise

    def select_task(self, ts_bgn, ts_end, title, person_id, ts_init):
        task = pd.read_sql(
            f'select * from task_scheduler.task where "ts_bgn" = \'{ts_bgn}\' and "ts_end" = \'{ts_end}\' and '
            f'"title" = \'{title}\' and "person_id" = \'{person_id}\' and "ts_init" = \'{ts_init}\';', self.conn)
        return self.get_task(convert_results_to_dict(task)["task_id"])

    def check_available_token(self, token):
        user = self.select_user_from_token(token)
        return not user.empty

    # Проверка токена
    def check_token(self, in_param=True):
        def inner(func):
            def wrapper(*args, **kwargs):
                token = request.args.get("token") if in_param else request.get_json().get("token")
                if token is None or not self.check_available_token(token):
                    return error_response(400, "Token not available")
                return func(*args, **kwargs)

            wrapper.__name__ = func.__name__
            return wrapper

        return inner

    def insert_new_registration_id(self, person_id, registration_id):
        with self.conn.begin() as tran:
            try:
                self.conn.execute(
                    f'insert into task_scheduler.registration_ids (id_person,registration_id) values ({person_id}, \'{registration_id}\');')
            except:
                tran.rollback()
                raise

    def select_person_registration_ids(self, person_id):
        ids = pd.read_sql(
            f'select "registration_id" from task_scheduler.registration_ids where "id_person" = \'{person_id}\';',
            self.conn)
        return ids

    def registration_id_is_exist(self, person_id, registration_id) -> bool:
        ids = [i["registration_id"] for i in convert_results_to_list_dict(self.select_person_registration_ids(person_id))]
        return registration_id in ids

    def get_registrations_ids_from_list_id_persons(self, persons_ids: list) -> list:
        reg_ids_persons = []
        for id in persons_ids:
            reg_ids_persons += [i["registration_id"] for i in
                                convert_results_to_list_dict(self.select_person_registration_ids(id))]
        reg_ids_persons = list(set(reg_ids_persons))
        return reg_ids_persons

    def get_all_registrations_ids(self) -> list:
        all_person_ids = [person["person_id"] for person in convert_results_to_list_dict(self.select_all_person())]
        all_ids = self.get_registrations_ids_from_list_id_persons(all_person_ids)
        return all_ids

    def get_all_registrations_ids_from_task(self, task_id: int) -> list:
        persons = convert_results_to_list_dict(self.select_persons_from_task(task_id))
        persons_id = [i["person_id"] for i in persons]
        return self.get_registrations_ids_from_list_id_persons(persons_id)

    def get_executors_registrations_ids_from_task(self, task_id: int) -> list:
        executors_ids = [i["person_id"] for i in list(
            filter(lambda x: x["role_id"] == 2, convert_results_to_list_dict(self.select_persons_from_task(task_id))))]
        return self.get_registrations_ids_from_list_id_persons(executors_ids)

    def get_boss_registrations_ids_from_task(self, task_id: int) -> list:
        executors_ids = [i["person_id"] for i in list(
            filter(lambda x: x["role_id"] == 1, convert_results_to_list_dict(self.select_persons_from_task(task_id))))]
        return self.get_registrations_ids_from_list_id_persons(executors_ids)

    def count_person_task(self, person_id) -> int:
        count = pd.read_sql(
                    f'SELECT COUNT(task_id) FROM task_scheduler.person_task WHERE "person_id" = {person_id};', self.conn)
        return convert_results_to_dict(count)["count"]

    def count_task_where_executor(self, person_id) -> int:
        count = pd.read_sql(
                    f'SELECT COUNT(task_id) FROM task_scheduler.person_task WHERE "person_id" = {person_id} and "role_id" = 2;',
                    self.conn)
        return convert_results_to_dict(count)["count"]

    def count_task_where_boss(self, person_id) -> int:
        count = pd.read_sql(
            f'SELECT COUNT(task_id) FROM task_scheduler.person_task WHERE "person_id" = {person_id} and "role_id" = 1;',
            self.conn)
        return convert_results_to_dict(count)["count"]

    def delete_person_task(self, person_id, task_id):
        with self.conn.begin() as tran:
            try:
                self.conn.execute(
                    f'delete from task_scheduler.person_task where "person_id" = {person_id} and "task_id" = {task_id};')
            except:
                tran.rollback()
                raise

    def update_role_id_person_task(self, person_id, task_id, role_id):
        with self.conn.begin() as tran:
            try:
                self.conn.execute(
                    f'update task_scheduler.person_task set "role_id" = {role_id} where "person_id" = {person_id} and "task_id" = {task_id};',
                    self.conn)
            except:
                tran.rollback()
                raise
