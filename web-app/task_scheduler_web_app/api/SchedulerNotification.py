from .send_notification import SenderNotification
from ..utils import *
import schedule
import time
import datetime as dt


class SchedulerNotification:
    def __init__(self, sender: SenderNotification):
        self.sender = sender
        self.request = sender.request_db

    @mult_threading
    def init_background_notifications(self):
        schedule.every().day.at("08:30").do(self.sender.send_statistic_notifications, is_morning=True)
        schedule.every().day.at("17:30").do(self.sender.send_statistic_notifications, is_morning=False)
        schedule.every().day.at("09:00").do(self.check_deadline_soon_task)
        while True:
            schedule.run_pending()
            time.sleep(1)

    def check_deadline_soon_task(self):
        for day in range(0, 3):
            start_date = (dt.datetime.now() + dt.timedelta(days=[0, 2, 4][day])).strftime("%Y-%m-%d")
            end_date = (dt.datetime.now() + dt.timedelta(days=[1, 3, 5][day])).strftime("%Y-%m-%d")
            soon_deadline_tasks = self.request.select_tasks_between_time(start_date, end_date)
            ids_tasks = [task["task_id"] for task in soon_deadline_tasks]
            for id_task in ids_tasks:
                reg_ids = self.request.get_all_registrations_ids_from_task(id_task)
                self.sender.send_notification_deadline_soon_task(id_task, reg_ids)