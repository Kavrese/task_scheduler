import random
from flask import jsonify, request, url_for, g, abort, app
from . import bp, sender_notification, request_db
from .request_to_db import *
from ..utils import *
from .send_notification import *


@bp.route('/persons', methods=['GET'])
@required_params(params={"token": "param", "idTask": "param"})
@request_db.check_token()
def get_persons_from_task():
    # Получение всех исполнителей задачи
    token = request.args.get('token', type=str)
    id_task = request.args.get('idTask', type=str)
    persons = request_db.select_persons_from_task(id_task)
    return jsonify(convert_results_to_list_dict(persons))


@bp.route('/person_task', methods=['GET'])
@required_params(params={"token": "param", "person_id": "param"})
@request_db.check_token()
def get_person_task():
    # Получение персональных задач пользователя
    token = request.args.get('token', type=str)
    person_id = request.args.get('person_id', type=str)
    role = request.args.get("role", type=str, default="all")
    id_tasks = list(set([i["task_id"] for i in convert_results_to_list_dict(
        request_db.select_person_task_with_role(person_id, int(role)) if role != "all" else request_db.select_person_task_role(person_id)
    )]))
    task = [request_db.get_task(id) for id in id_tasks if request_db.task_is_exist(id)]
    return jsonify(task)


@bp.route('/all_person', methods=['GET'])
@required_params(params={"token": "param"})
@request_db.check_token()
def get_all_persons():
    # Получение абсалютно всех пользователей
    token = request.args.get('token', type=str)
    persons = request_db.select_all_person()
    return jsonify(convert_results_to_list_dict(persons))


@bp.route('/task/new_status', methods=['POST'])
@required_params(params={'token': "param", "idTask": "param", "newStatus": "param"})
@request_db.check_token()
def set_new_status():
    # Метод изменения статуса у задачи
    token = request.args.get('token', type=str)
    id_task = request.args.get('idTask', type=str)
    new_status = request.args.get('newStatus', type=int)
    request_db.update_status(new_status, id_task)
    task = request_db.get_task(id_task)
    create_event(token, id_task, details=str({"description": "Статус изменён"}))

    sender_notification.send_notification_for_status(new_status, id_task)
    return jsonify(task)


@bp.route('/task', methods=['GET'])
@required_params(params={'token': "param"})
@request_db.check_token()
def get_task_between_time():
    # Получение всех задач в определённом промежутке
    token = request.args.get('token', type=str)
    end_time = request.args.get('endTime', type=str, default="9999-12-31")
    start_time = request.args.get('startTime', type=str, default=end_time if end_time != "9999-12-31" else "1-1-1")
    tasks = request_db.select_tasks_between_time(start_time, end_time)
    return jsonify(tasks)


@bp.route('/task_person', methods=['GET'])
@required_params(params={'token': "param"})
@request_db.check_token()
def get_task_between_time_person():
    # Получение задач пользователя в определённом промежутке
    token = request.args.get('token', type=str)
    id_person = request.args.get('id_person', type=str, default=request_db.get_user_id_from_token(token))
    end_time = request.args.get('endTime', type=str, default="9999-12-31")
    start_time = request.args.get('startTime', type=str, default=end_time if end_time != "9999-12-31" else "1-1-1")
    tasks = request_db.select_person_tasks_between_time(id_person, start_time, end_time)
    return jsonify(tasks)


@bp.route('/task', methods=['POST'])
@required_params(params={'token': "param", "ts_bgn": "body", "ts_end": "body", "title": "body", "args": "body",
                         "persons_role_2": "body", "persons_role_1": "body"})
@request_db.check_token()
def create_new_task():
    # Создание новой задачи
    json = request.get_json()
    token = request.args.get("token")
    ts_bgn = json["ts_bgn"]
    ts_end = json["ts_end"]
    persons_role_2 = [str(person["person_id"]) for person in json["persons_role_2"]]
    persons_role_1 = [str(person["person_id"]) for person in json["persons_role_1"]]
    person_id = request_db.get_user_id_from_token(token)
    title = json["title"]
    status = 0
    args = json["args"] if "args" in json else dict()
    ts_init = get_system_datetime()
    request_db.insert_task(ts_bgn, ts_end, ts_init, person_id, title, convert_dict_to_str(args), status)
    task = request_db.select_task(ts_bgn, ts_end, title, person_id, ts_init)
    task_id = task["task_id"]
    for executor in persons_role_2:
        request_db.insert_person_task(task_id, executor, 2)
    for boss in persons_role_1:
        request_db.insert_person_task(task_id, boss, 1)
    request_db.insert_event(task_id, person_id, ts_init, convert_dict_to_str({"description": "Задание создано"}))

    # Рассылка уведомлений
    executors_reg_ids = request_db.get_registrations_ids_from_list_id_persons(persons_role_2)
    bosses_reg_ids = request_db.get_registrations_ids_from_list_id_persons(persons_role_1)
    sender_notification.send_notification_persons_new_task(task_id, executors_reg_ids)
    sender_notification.send_notification_add_task(task_id, bosses_reg_ids)

    return jsonify(task)


@bp.route('/update_task', methods=['POST'])
@required_params(
    params={'token': "param", "task_id": "body", "ts_bgn": "body", "ts_end": "body", "title": "body", "args": "body",
            "persons_role_2": "body", "persons_role_1": "body"})
@request_db.check_token()
def update_task():
    # Обновление задачи
    json = request.get_json()
    token = request.args.get("token")
    ts_bgn = json["ts_bgn"]
    ts_end = json["ts_end"]
    executors = [str(person["person_id"]) for person in json["persons_role_2"]]
    bosses = [str(person["person_id"]) for person in json["persons_role_1"]]
    task_id = json["task_id"]
    person_id = request_db.get_user_id_from_token(token)
    title = json["title"]
    args = json["args"] if "args" in json else dict()

    task = request_db.get_task(task_id)
    old_executors_id = request_db.get_ids_person_role(task_id, 2)
    old_bosses_id = request_db.get_ids_person_role(task_id, 1)
    new_bosses = list(set(bosses) - set(old_bosses_id))
    new_executors = list(set(executors) - set(old_executors_id))

    for executor in old_executors_id:
        ids_executor = [i["registration_id"] for i in
                        convert_results_to_list_dict(request_db.select_person_registration_ids(executor))]
        if executor not in executors:
            if executor in bosses:
                sender_notification.send_notification_now_boss(task_id, request_db.select_person_registration_ids(executor))
                request_db.update_role_id_person_task(executor, task_id, 1)
            else:
                sender_notification.send_notification_now_no_work(task_id, request_db.select_person_registration_ids(executor))
                request_db.delete_person_task(executor, task_id)

    for boss in old_bosses_id:
        ids_boss = [i["registration_id"] for i in convert_results_to_list_dict(request_db.select_person_registration_ids(boss))]
        if boss not in bosses:
            if boss in executors:
                sender_notification.send_notification_now_executor(task_id, request_db.select_person_registration_ids(boss))
                request_db.update_role_id_person_task(boss, task_id, 2)
            else:
                sender_notification.send_notification_now_no_work(task_id, request_db.select_person_registration_ids(boss))
                request_db.delete_person_task(boss, task_id)

    for new_boss in new_bosses:
        ids_boss = [i["registration_id"] for i in
                    convert_results_to_list_dict(request_db.select_person_registration_ids(new_boss))]
        request_db.insert_person_task(task_id, int(new_boss), 1)
        sender_notification.send_notification_add_task(task_id, ids_boss)

    for new_executor in new_executors:
        ids_executor = [i["registration_id"] for i in
                        convert_results_to_list_dict(request_db.select_person_registration_ids(new_executor))]
        request_db.insert_person_task(task_id, int(new_executor), 2)
        sender_notification.send_notification_add_task(task_id, ids_executor)
    if task["title"] != title or task["args"] != args: #or task["ts_bgn"] != convert_string_to_datetime(ts_bgn) or task["ts_end"] != convert_string_to_datetime(ts_bgn):
        sender_notification.send_notification_change_task(task_id, request_db.get_all_registrations_ids_from_task(task_id))
        if task["title"] != title:
            request_db.update_task_column("title", title, task_id)
        if task["args"] != args:
            request_db.update_task_column("args", convert_dict_to_str(args), task_id)
    request_db.update_task_column("ts_bgn", ts_bgn, task_id)
    request_db.update_task_column("ts_end", ts_end, task_id)
    #senderNotification.send_notification_new_deadline_task(task_id, request_db.get_all_registrations_ids_from_task(task_id))

    request_db.insert_event(task_id, person_id, task["ts_init"], convert_dict_to_str({"description": "Задание обновлено"}))
    task = request_db.get_task(task_id)
    return jsonify(task)


@bp.route('/search', methods=['GET'])
@required_params(params={'token': "param", "filter": "param", "textSearch": "param"})
@request_db.check_token()
def search_with_filter():
    # Поиск
    token = request.args.get('token', type=str)
    text_search = request.args.get('textSearch', type=str)
    filter_search = request.args.get('filter', type=str, default="person")
    tasks = request_db.select_search(filter_search, text_search)
    return jsonify(tasks)


@bp.route('/task/id', methods=['GET'])
@required_params(params={'token': "param", "idTask": "param"})
@request_db.check_token()
def get_task_with_id():
    # Получение задачи по id
    token = request.args.get('token', type=str)
    id_task = request.args.get('idTask', type=str)
    if not request_db.task_is_exist(id_task):
        return error_response(404, "Task not found")
    task = request_db.get_task(id_task)
    return jsonify(task)


@bp.route('/login', methods=['POST'])
@required_params(params={'email': "body", 'password': "body"})
def login():
    # Авторизация
    body = request.get_json()
    phone = body.get('phone')
    email = body.get('email')
    password = body.get('password')
    token = random.randint(100000, 1000000)
    user = request_db.select_user(email, password)
    if not user.empty:
        # Изменение токен у пользователя с такими данными
        request_db.insert_new_token(token, email, password)
    else:
        return error_response(404, message="User not found")
    user = convert_clear_user_results_to_dict(user)
    user['token'] = token
    return jsonify(user)


@bp.route('/signup', methods=['POST'])
@required_params(params={'email': "body", 'password': "body",
                         "phone": "body", "firstname": "body",
                         "secondname": "body", "lastname": "body",
                         "person_level": "body"})
def signup():
    # Регистрация
    body = request.get_json()
    phone = body.get('phone')
    email = body.get('email')
    password = body.get('password')
    firstname = body.get('firstname')
    secondname = body.get('secondname')
    person_level = body.get('person_level')
    lastname = body.get('lastname')
    token = random.randint(100000, 1000000)

    request_db.insert_user(token, phone, email, password, firstname, lastname, secondname, person_level)
    result = request_db.select_user(email, password)
    if not result.empty:
        return jsonify(convert_clear_user_results_to_dict(result))
    else:
        return error_response(404, "User not created")


@bp.route('/person/id', methods=['GET'])
@required_params(params={"token": "param", "personId": "param"})
@request_db.check_token()
def person_from_id():
    # Получение пользователя по id
    token = request.args.get('token', type=str)
    person_id = request.args.get('personId', type=str)
    user = request_db.select_user_from_id(person_id)
    return jsonify(convert_results_to_dict(user))


@bp.route('/registration_id', methods=['POST'])
@required_params(params={"token": "param", "registration_id": "param"})
@request_db.check_token()
def new_registration_id():
    # Новый registration_id пользователя
    token = request.args.get('token', type=str)
    registration_id = request.args.get('registration_id', type=str)
    person_id = convert_results_to_dict(request_db.select_user_from_token(token))["person_id"]
    ids = [i["registration_id"] for i in convert_results_to_list_dict(request_db.select_person_registration_ids(person_id))]
    if request_db.registration_id_is_exist(person_id, registration_id):
        return jsonify(ids)
    request_db.insert_new_registration_id(person_id, registration_id)
    ids.append(registration_id)
    return jsonify(ids)


@bp.route('/registration_id', methods=['GET'])
@required_params(params={"token": "param"})
@request_db.check_token()
def registration_ids():
    # Получение registration_ids пользователя
    token = request.args.get('token', type=str)
    person_id = convert_results_to_dict(request_db.select_user_from_token(token))["person_id"]
    ids = [i["registration_id"] for i in convert_results_to_list_dict(request_db.select_person_registration_ids(person_id))]
    return jsonify(ids)


@bp.route('/event', methods=['POST'])
@required_params(params={"token": "body", "idTask": "body", "details": "body"})
@request_db.check_token(False)
def create_event_api():
    # Создание нового лога
    token = request.args.get('token', type=str)
    id_task = request.args.get('idTask', type=str)
    details = request.args.get('details', type=str)
    create_event(token, id_task, details)
    return jsonify([])


def create_event(token, id_task, details):
    user = convert_results_to_dict(request_db.select_user_from_token(token))
    id_person = user["person_id"]
    ts_ = get_system_datetime()
    request_db.insert_event(id_task, id_person, ts_, details)