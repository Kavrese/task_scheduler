from setuptools import setup  # , find_packages

setup(
    name='invest_terminal',
    version='2021.7.18.0.0',
    packages=['invest_terminal'],
    author='M.S.Pogodin',
    author_email='m.s.pogodin@gmail.com',
    install_requires=['Flask', 'WTForms', 'PyJWT', 'Werkzeug', 'Flask-SQLAlchemy', 'Flask-WTF', 'Flask-Login', 'Flask-HTTPAuth', 'Flask-Bootstrap', "pyfcm", "schedule"]
)
