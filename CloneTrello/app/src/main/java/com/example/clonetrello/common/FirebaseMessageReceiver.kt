package com.example.clonetrello.common
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.util.Log
import com.example.clonetrello.sendNotification
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import android.content.Intent
import com.example.clonetrello.AuthScreen.AuthActivity
import com.example.clonetrello.TaskScreen.TaskActivity
import com.example.clonetrello.initRetrofit


class MyFirebaseMessagingService : FirebaseMessagingService() {

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        Log.d(TAG, "From: ${remoteMessage.from}")
        remoteMessage.notification.apply {
            var pendingIntent: PendingIntent? = null
            if (this != null) {
                if (remoteMessage.data.isNotEmpty()) {
                    val body = remoteMessage.data
                    val type = body["type"]
                    if (type == "for_task"){
                        val notificationIntent = if (getSharedPreferences("0", 0).getString("person_id", null) == null) Intent(
                            applicationContext,
                            AuthActivity::class.java
                        ) else Intent(
                            applicationContext,
                            TaskActivity::class.java
                        )
                        notificationIntent.putExtra("taskId", body["id_task"]!!.toInt())
                        notificationIntent.flags =  Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP
                        pendingIntent = PendingIntent.getActivity(applicationContext, 105, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT)
                    }
                }
                Log.d(TAG, "Message Notification Body: ${this.body}")
                (this@MyFirebaseMessagingService.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager)
                    .sendNotification(this@MyFirebaseMessagingService, this.body!!, this.title!!, pendingIntent = pendingIntent)
            }else{
                Log.d(TAG, "Message Notification Body Null")
            }
        }
    }

    override fun onNewToken(token: String) {
        Log.d(TAG, "Refreshed token: $token")
    }

    companion object {
        private const val TAG = "FirebaseMsgService"
    }
}