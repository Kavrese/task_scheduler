package com.example.clonetrello

import android.app.AlertDialog
import com.example.clonetrello.AuthScreen.AuthActivity
import com.example.clonetrello.common.Api
import com.example.clonetrello.common.Models.ModelPersonFromTask
import com.example.clonetrello.common.Models.ModelUser
import com.example.clonetrello.common.OnGetExecutors
import com.example.clonetrello.common.interfaces.OnParseIdPerson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.media.RingtoneManager
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import com.example.clonetrello.common.ConnectControllerAcceptPoint
import com.google.firebase.messaging.FirebaseMessaging

fun countDays(task_date: String): Int{
    val dateNow = Calendar.getInstance()
    val calendarDateTask = Calendar.getInstance()
    dateNow.time = Date()
    calendarDateTask.time = SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss", Locale.ENGLISH).parse(task_date)!!
    return TimeUnit.DAYS.convert(calendarDateTask.timeInMillis - dateNow.timeInMillis, TimeUnit.MILLISECONDS).toInt()
}

fun initRetrofit(): Api{
    return ConnectControllerAcceptPoint.connectController.retrofitServer
}

fun showDialog(context: Context, title:String, message: String){
    AlertDialog.Builder(context, R.style.AlertDialogTheme)
        .setTitle(title)
        .setMessage(message)
        .setNegativeButton("OK", null)
        .show()
}

fun showDialog(context: Context, title:String){
    AlertDialog.Builder(context, R.style.AlertDialogTheme)
        .setTitle(title)
        .setMessage("Повторите попытку позже !")
        .setNegativeButton("OK", null)
        .show()
}

fun showDialog(context: Context, title:String, t: Throwable){
    AlertDialog.Builder(context, R.style.AlertDialogTheme)
        .setTitle(title)
        .setMessage(t.message)
        .setNegativeButton("OK", null)
        .show()
}

fun parseData(date: String): String{
    val date_date = SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss", Locale.ENGLISH).parse(date)!!
    val date_str = SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH).format(date_date)
    return date_str
}

fun parseDataFromString(date: String): Date{
    val date_date = SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss", Locale.ENGLISH).parse(date)!!
    return date_date
}

fun parseDataToString(date: Date): String{
    val date_str = SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss", Locale.ENGLISH).format(date)
    return date_str
}

fun toShortDateFormat(date: Date): String{
    val date_str = SimpleDateFormat("EEE, dd/MM/yy", Locale.ENGLISH).format(date)
    return date_str
}

fun toShortDateFormat(date: String): String{
    val date_date = SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss", Locale.ENGLISH).parse(date)
    val date_str = SimpleDateFormat("EEE, dd/MM/yy", Locale.ENGLISH).format(date_date!!)
    return date_str
}

fun toLongFromShortString(date: String): String{
    val short_date = SimpleDateFormat("EEE, dd/MM/yy", Locale.ENGLISH).parse(date)
    val long_str = SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss", Locale.ENGLISH).format(short_date!!)
    return long_str
}

fun parseIntToStringStatus(status: Int): String{
    return when(status){
        -2 -> "Задача просрочена"
        -1 -> "Задача отменена"
        //0 -> "Задача выдана"
        0, 1 -> "В работе"
        2 -> "Ожидает подтверждения"
        3 -> "Выполнено"
        else -> "Неизвестно"
    }
}

fun clearLocalUserData(context: Context){
    context.getSharedPreferences("0", 0).edit()
        .putString("email", null)
        .putString("password", null)
        .putString("person_id", null)
        .apply()
}

fun generateWordsForCountDay(countDay: String): Pair<String, String>{
    val last = if (countDay == "1" || countDay == "-1") "Прошёл" else "Прошло"
    val after = if (countDay[countDay.length - 1] == '1' && "11" !in countDay) "день" else
        if (countDay[countDay.length - 1].toString().toInt() in 2..4) "дня" else "дней"
    return Pair(last, after)
}

fun myCeil(num: Double): Int{
    val num_int = num.toInt()
    return if (num_int.toDouble() != num) num_int + 1 else num_int
}

fun parseIdPerson(idPerson: String, onParse: OnParseIdPerson){
    initRetrofit().person(idPerson).enqueue(object:
        Callback<ModelUser> {
        override fun onResponse(call: Call<ModelUser>, response: Response<ModelUser>) {
            if (response.body() != null){
                val body = response.body()!!
                onParse.onParse(convertModelUserToStrFIO(body))
            }else{
                onParse.onParse(idPerson)
            }
        }

        override fun onFailure(call: Call<ModelUser>, t: Throwable) {}
    })
}

fun convertModelUserToStrFIO(body: ModelUser): String{
    return "${body.secondname.substring(0, 1).uppercase()}${body.secondname.substring(1)} ${body.firstname[0].uppercase()}. ${body.lastname[0].uppercase()}."
}

fun getExecutorsTask(context: Context, idTask: String, onGetExecutors: OnGetExecutors){
    initRetrofit().getExecutorTask(idTask).enqueue(object: Callback<List<ModelPersonFromTask>>{
        override fun onResponse(
            call: Call<List<ModelPersonFromTask>>,
            response: Response<List<ModelPersonFromTask>>
        ) {
           if (response.body() != null){
               onGetExecutors.getExecutors(response.body()!!)
           }else{
               showDialog(context, "Ошибка получения исполнителей задачи")
           }
        }

        override fun onFailure(call: Call<List<ModelPersonFromTask>>, t: Throwable) {
            showDialog(context, "Ошибка получения исполнителей задачи", t)
        }
    })
}

fun FirebaseMessaging.sendTokenToAPI(token: String){
    getToken().addOnCompleteListener { task ->
        if (!task.isSuccessful()) {
            Log.w("SendTokenToAPI", "Fetching FCM registration token failed", task.exception);
            return@addOnCompleteListener
        }
        val reg_id = task.result
        initRetrofit().newRegId(token, reg_id).enqueue(object:Callback<List<String>>{
            override fun onResponse(call: Call<List<String>>, response: Response<List<String>>) {
                Log.w("SendTokenToAPI", "Good send reg id to API", task.exception);
            }

            override fun onFailure(call: Call<List<String>>, t: Throwable) {
                Log.w("SendTokenToAPI", "Fail send reg id to API", task.exception);
            }
        })
    }
}

fun NotificationManager.sendNotification(
    context: Context,
    messageBody: String,
    title: String = context.getString(R.string.app_name),
    channelName: String = "Задачи",
    channelId: String = "TaskSchedulerAppIdChannel",
    idNotification: Int = generateNotificationIdFromDate(),
    pendingIntent: PendingIntent? = null,
    autoCancel: Boolean = true,
    smallIconId: Int = R.drawable.icon_notification,
    color_icon: Int = ContextCompat.getColor(context, R.color.colorAccentDark)
) {
    val mPendingIntent = if (pendingIntent == null) {
        val intent = Intent(context, AuthActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        PendingIntent.getActivity(
            context, 0 /* Request code */, intent,
            PendingIntent.FLAG_ONE_SHOT
        )
    } else {
        pendingIntent
    }
    val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
    val notificationBuilder = NotificationCompat.Builder(context, channelId)
        .setSmallIcon(smallIconId)
        .setContentTitle(title)
        .setContentText(messageBody)
        .setAutoCancel(autoCancel)
        .setColor(color_icon)
        .setStyle(NotificationCompat.BigTextStyle())
        .setSound(defaultSoundUri)
        .setContentIntent(mPendingIntent)
    // Since android Oreo notification channel is needed.
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
        val channel =  NotificationChannel(
            channelId,
            channelName,
            NotificationManager.IMPORTANCE_DEFAULT
        )
        createNotificationChannel(channel)
    }
    notify(idNotification, notificationBuilder.build())
}

private fun generateNotificationIdFromDate(): Int {
    return (Date().time / 1000L % Int.MAX_VALUE).toInt()
}