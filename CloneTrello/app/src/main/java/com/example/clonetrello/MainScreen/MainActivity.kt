 package com.example.clonetrello.MainScreen

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.widget.PopupMenu
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.viewpager2.widget.ViewPager2
import com.example.clonetrello.*
import com.example.clonetrello.Adapters.AdapterDatesGroup
import com.example.clonetrello.Adapters.AdapterDatesSingle
import com.example.clonetrello.Adapters.AdapterTask
import com.example.clonetrello.CreateEditTaskScreen.CreateEditTaskActivity
import com.example.clonetrello.ProfileScreen.ProfileActivity
import com.example.clonetrello.TaskScreen.TaskActivity
import com.example.clonetrello.common.Info
import com.example.clonetrello.common.Models.ModelCardDate
import com.example.clonetrello.common.Models.ModelTask
import com.example.clonetrello.common.ToTaskScreen
import com.example.clonetrello.common.interfaces.*
import kotlinx.android.synthetic.main.activity_auth.*
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_main.pager_date_single
import kotlinx.android.synthetic.main.activity_main.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

lateinit var updateDataScreen: View.OnClickListener

 class MainActivity : AppCompatActivity() {
    var singleMode = true
    lateinit var listModelCardDate: MutableList<ModelCardDate>
    lateinit var onClickTask: OnClickTask
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        updateDataScreen = View.OnClickListener { updateData() }

        rec_date_group.visibility = View.GONE

        onClickTask = object: OnClickTask {
            override fun onClick(modelTask: ModelTask) {
                ToTaskScreen.modelTask = modelTask
                val intent = Intent(this@MainActivity, TaskActivity::class.java)
                intent.putExtra("taskId", modelTask.task_id)
                startActivity(intent)
            }
        }

        updateData()
        initSearch()

        mode.setOnClickListener {
            switchMode()
        }

        user.setOnClickListener {
            startActivity(Intent(this, ProfileActivity::class.java))
        }

        add_task.setOnClickListener {
            ToTaskScreen.modelTask = null
            CreateEditTaskActivity.ToActivity.onGetTask = object: OnGetTask{
                override fun onGood(modelTask: ModelTask) {
                    updateData()
                }
            }
            startActivity(Intent(this, CreateEditTaskActivity::class.java))
        }

        refresh.setOnClickListener {
            updateData()
        }
    }

    private fun updateData(){
        showMainWindow()
        (if (Info.modelUser!!.person_level == 1) initRetrofit().getTasksPerson() else initRetrofit().getTasksPerson()).enqueue(object: Callback<List<ModelTask>>{
            override fun onResponse(
                call: Call<List<ModelTask>>,
                response: Response<List<ModelTask>>
            ) {
                if (response.body() != null) {
                    setDatesCardFromListTask(response.body()!!)
                }else{
                    showDialog(this@MainActivity, "Ошибка получения задач")
                }
                hideMainWindow()
            }

            override fun onFailure(call: Call<List<ModelTask>>, t: Throwable) {
                showDialog(this@MainActivity, "Ошибка получения задач", t)
                hideMainWindow()
            }

        })
    }

     private fun setDatesCardFromListTask(listTasks: List<ModelTask>){
         listModelCardDate = getListModelCardDateFromListTasks(listTasks).toMutableList()

         initPager()
         initRec()
         scrollToNearestDate()
     }

     //Метод прокрутки pager до ближайшей даты (по модулю)
     private fun scrollToNearestDate() {
         val positionNearestCard = getNearestDateCardPosition()
         pager_date_single.setCurrentItem(positionNearestCard, false)
     }

     private fun getListModelCardDateFromListTasks(listTasks: List<ModelTask>): List<ModelCardDate>{
         listModelCardDate = arrayListOf()

         //Спиок всех дат
         val setListDate = getListAllDateStringFromListTask(listTasks)

         setListDate.forEach{ date_ts_end ->
             //Получаем все задачи с этой датой
             val allTaskWithThisDate = listTasks.filter {it.ts_end == date_ts_end}
             //Добавляем все задачи в одну карточку даты
             listModelCardDate.add(ModelCardDate(date_ts_end, allTaskWithThisDate))
         }
         return listModelCardDate
     }

     private fun getListAllDateStringFromListTask(listTasks: List<ModelTask>): List<String>{
         return listTasks.map{it.ts_end!!}.toSet().toList().sortedWith(compareBy{parseDataFromString(it)})
     }

     private fun getListAllDateStringFromListModelCardDate(listDates: List<ModelCardDate>): List<String>{
         return listDates.map{modelCard -> modelCard.date}
     }

     private fun initPager(){
         val listInterface: MutableList<OnNewPosition> = arrayListOf()
         pager_date_single.adapter = AdapterDatesSingle(listModelCardDate, object:
             OnNewInterface {
             override fun new_interface(onNewPosition: OnNewPosition) {
                 listInterface.add(onNewPosition)
             }
         }, onClickTask)
         pager_date_single.registerOnPageChangeCallback(object: ViewPager2.OnPageChangeCallback() {
             override fun onPageSelected(position: Int) {
                 listInterface.forEach { onNewPosition: OnNewPosition -> onNewPosition.position(position)}
             }
         })
     }

     private fun initRec(){
         rec_date_group.apply {
             adapter = AdapterDatesGroup(listModelCardDate, object: OnClickDateGroup {
                 override fun onClick(pos: Int) {
                     this@MainActivity.motion_swipe_dates.transitionToStart()
                     this@MainActivity.rec_date_group.scrollToPosition(0)
                     switchMode()
                     this@MainActivity.pager_date_single.setCurrentItem(pos, false)
                 }
             })
             layoutManager = GridLayoutManager(this@MainActivity, 2)
         }
     }

     private fun getNearestDateCardPosition(): Int{
         val listAllDates = getListAllDateStringFromListModelCardDate(listModelCardDate)

         var minDays = -1
         var nearestDate = ""

         for (date in listAllDates.reversed()) {
             val countDays = kotlin.math.abs(countDays(date))
             if (countDays < minDays || minDays == -1){
                 nearestDate = date
                 minDays = countDays
             }else{
                 break
             }
         }

         return listAllDates.indexOf(nearestDate)
     }

     private fun initSearch(){
         filter_search.text = "Человек"
         val popupMenu = PopupMenu(this, filter_search)
         popupMenu.inflate(R.menu.menu_filter)
         popupMenu.setOnMenuItemClickListener {
             filter_search.text = it.title
             return@setOnMenuItemClickListener true
         }
         filter_search.setOnClickListener {
            popupMenu.show()
         }
//         search.setOnFocusChangeListener { v, hasFocus ->
//             filter_search.visibility = if (hasFocus) View.VISIBLE else View.GONE
//         }

         search.setOnKeyListener(View.OnKeyListener { v, keyCode, event ->
             return@OnKeyListener if (keyCode == KeyEvent.KEYCODE_ENTER){
                 searchAllTask(search.text.toString())
                 true
             }else{
                 false
             }
         })
         clear_search.setOnClickListener {
             clearSearch()
         }
     }

     private fun clearSearch(){
         rec_search.visibility = View.GONE
         add_task.visibility = View.VISIBLE
         clear_search.visibility = View.GONE
         search.setText("")
         if (singleMode)
             pager_date_single.visibility = View.VISIBLE
         else
             rec_date_group.visibility = View.VISIBLE
     }

     private fun initResultSearchTask(listTask: List<ModelTask>){
         rec_search.visibility = View.VISIBLE
         if (singleMode)
             pager_date_single.visibility = View.GONE
         else
             rec_date_group.visibility = View.GONE
         rec_search.apply {
             layoutManager = LinearLayoutManager(this@MainActivity)
             adapter = AdapterTask(listTask, onClickTask)
         }
         add_task.visibility = View.GONE
         clear_search.visibility = View.VISIBLE
     }

     private fun searchAllTask(task: String): List<ModelTask>{
         val listTask: MutableList<ModelTask> = arrayListOf()
         initRetrofit().searchTask(task).enqueue(object: Callback<List<ModelTask>>{
             override fun onResponse(
                 call: Call<List<ModelTask>>,
                 response: Response<List<ModelTask>>
             ) {
                 if (response.body() != null){
                     initResultSearchTask(response.body()!!)
                 }else{
                     com.example.clonetrello.showDialog(this@MainActivity, "Ошибка поиска")
                 }
             }

             override fun onFailure(call: Call<List<ModelTask>>, t: Throwable) {
                 com.example.clonetrello.showDialog(this@MainActivity, "Ошибка поиска", t)
             }

         })
         return listTask
     }

    private fun switchMode(){
        clearSearch()
        singleMode = !singleMode
        if (singleMode) {
            mode.setImageResource(R.drawable.ic_mode_single)
            pager_date_single.visibility = View.VISIBLE
            rec_date_group.visibility = View.GONE
        }else {
            mode.setImageResource(R.drawable.ic_mode_group)
            pager_date_single.visibility = View.GONE
            rec_date_group.visibility = View.VISIBLE
        }
    }

     private fun showMainWindow(){
         window_main.visibility = View.VISIBLE
     }

     private fun hideMainWindow(){
         window_main.visibility = View.GONE
     }
}