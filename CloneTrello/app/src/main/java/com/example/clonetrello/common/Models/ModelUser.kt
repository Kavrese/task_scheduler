package com.example.clonetrello.common.Models

data class ModelUser(
    var email: String,
    var firstname: String,
    var lastname: String,
    var person_id: Int,
    var person_level: Int,
    var phone: String,
    var position: String,
    var secondname: String,
    var token: String
)