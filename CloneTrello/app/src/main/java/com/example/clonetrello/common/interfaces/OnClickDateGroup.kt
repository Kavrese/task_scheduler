package com.example.clonetrello.common.interfaces

interface OnClickDateGroup {
    fun onClick(pos: Int)
}