package com.example.clonetrello.common.Models

data class ModelCardDate(
    val date: String,
    val list_task: List<ModelTask>,
)
