package com.example.clonetrello.common

import com.example.clonetrello.common.Models.*
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

interface Api {
    @POST("login")
    fun login(@Body body: ModelLoginBody): Call<ModelUser>

    @GET("task")
    fun allTask(@Query("token") token: String = Info.modelUser!!.token.toString()): Call<List<ModelTask>>

    @GET("task/id")
    fun taskId(@Query("token") token: String = Info.modelUser!!.token.toString(), @Query("idTask") taskId: Int): Call<ModelTask>

    @GET("persons")
    fun getExecutorTask(@Query("idTask") idTask: String, @Query("token") token: String = Info.modelUser!!.token.toString()): Call<List<ModelPersonFromTask>>

    @GET("person/id")
    fun person(@Query("personId") idPerson: String, @Query("token") token: String = Info.modelUser!!.token.toString()): Call<ModelUser>

    @GET("search")
    fun searchTask(@Query("textSearch") textSearch: String, @Query("filter") filter: String = "task", @Query("token") token: String = Info.modelUser!!.token.toString()): Call<List<ModelTask>>

    @GET("search")
    fun searchPerson(@Query("textSearch") textSearch: String, @Query("filter") filter: String = "person", @Query("token") token: String = Info.modelUser!!.token.toString()): Call<List<ModelUser>>

    @POST("task/new_status")
    fun newStatus(@Query("token") token: String = Info.modelUser!!.token.toString(), @Query("idTask") idTask: String, @Query("newStatus") newStatus: Int = 2): Call<ModelTask>

    @POST("task")
    fun createTask(@Query("token") token: String = Info.modelUser!!.token.toString(), @Body body: ModelTask): Call<ModelTask>

    @POST("update_task")
    fun updateTask(@Query("token") token: String = Info.modelUser!!.token.toString(), @Body body: ModelTask): Call<ModelTask>

    @GET("all_person")
    fun getAllPerson(@Query("token") token: String = Info.modelUser!!.token.toString()): Call<List<ModelUser>>

    @GET("person_task")
    fun getTasksPerson(@Query("token") token: String = Info.modelUser!!.token.toString(), @Query("person_id") personId: String = Info.modelUser!!.person_id.toString(), @Query("role") role: String = "all"): Call<List<ModelTask>>

    @POST("registration_id")
    fun newRegId(@Query("token") token: String = Info.modelUser!!.token.toString(), @Query("registration_id") registration_id: String): Call<List<String>>
}

interface FirebaseAPI{
    @GET("server.json")
    fun serverConnect(): Call<ModelServer>
}