package com.example.clonetrello.common.interfaces

interface OnParseIdPerson {
    fun onParse(parse: String)
}
