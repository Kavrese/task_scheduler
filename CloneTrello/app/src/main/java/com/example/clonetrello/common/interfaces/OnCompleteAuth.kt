package com.example.clonetrello.common.interfaces

import com.example.clonetrello.common.Models.ModelUser

interface OnCompleteAuth {
    fun onAuth(modelUser: ModelUser, password: String)
}