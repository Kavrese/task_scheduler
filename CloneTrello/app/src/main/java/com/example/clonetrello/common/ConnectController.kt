package com.example.clonetrello.common

import com.example.clonetrello.common.Models.ModelServer
import com.example.clonetrello.common.Models.ModelTask
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object ConnectControllerAcceptPoint{
    val connectController = ConnectController()
}

interface OnGetData<T> {
    fun onGetData(data: T)
    fun onFail(message: String)
}


class ConnectController {

    private val retrofitFirebase: FirebaseAPI = initRetrofit(URLS.FIREBASE.url).create(FirebaseAPI::class.java)
    lateinit var retrofitServer: Api

    companion object {
        fun initRetrofit(baseUrl: String): Retrofit{
            return Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
        }
    }

    fun connectToLocalHostIfExistElseServer(onGetData: OnGetData<String>){
        checkCreateRetrofitUrl(URLS.LOCAL_HOST.url, object: OnCheckConnection{
            override fun onCheck(check: Boolean, connectionApi: Api) {
                if (check){
                    retrofitServer = connectionApi
                    onGetData.onGetData(URLS.LOCAL_HOST.url)
                }else{
                    connectToServer(onGetData)
                }
            }
        })
    }

    fun connectToServer(onGetData: OnGetData<String>){
        retrofitFirebase.serverConnect().enqueue(object: Callback<ModelServer>{
            override fun onResponse(call: Call<ModelServer>, response: Response<ModelServer>) {
                if (response.isSuccessful){
                    val servers = response.body()!!
                    checkCreateRetrofitUrl(servers.main_server, object: OnCheckConnection{
                        override fun onCheck(check: Boolean, connectionApi: Api) {
                            retrofitServer = connectionApi
                            if (check)
                                onGetData.onGetData(servers.main_server)
                            else
                                onGetData.onGetData(servers.backup_server)
                        }
                    })
                }else{
                    onGetData.onFail("${response.code()} - body null")
                }
            }

            override fun onFailure(call: Call<ModelServer>, t: Throwable) {
                onGetData.onFail(t.message!!)
            }

        })
    }

    private fun checkCreateRetrofitUrl(url: String, onCheckConnection: OnCheckConnection){
        val testRetrofitServer = initRetrofit(url + "api/").create(Api::class.java)
        checkConnectionApi(testRetrofitServer, onCheckConnection)
    }

    private fun checkConnectionApi(api: Api, onCheckConnection: OnCheckConnection){
        api.allTask("").enqueue(object: Callback<List<ModelTask>>{
            override fun onResponse(
                call: Call<List<ModelTask>>,
                response: Response<List<ModelTask>>
            ) {
                onCheckConnection.onCheck(true, api)
            }

            override fun onFailure(call: Call<List<ModelTask>>, t: Throwable) {
                onCheckConnection.onCheck(false, api)
            }

        })
    }

    private interface OnCheckConnection{
        fun onCheck(check: Boolean, connectionApi: Api)
    }

    enum class URLS(var url: String){
        FIREBASE("https://exampledatabase-ef8d4-default-rtdb.firebaseio.com/"),
        LOCAL_HOST("http://192.168.1.64:4000/")
    }
}