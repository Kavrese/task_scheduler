package com.example.clonetrello.common.interfaces

import com.example.clonetrello.common.Models.ModelTask

interface OnClickTask {
    fun onClick(modelTask: ModelTask)
}