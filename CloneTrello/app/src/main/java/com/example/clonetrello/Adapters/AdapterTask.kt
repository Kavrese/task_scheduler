package com.example.clonetrello.Adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.clonetrello.common.Models.ModelTask
import com.example.clonetrello.R
import com.example.clonetrello.common.interfaces.OnClickTask

class AdapterTask(val listTask: List<ModelTask>, val onClickTask: OnClickTask? = null): RecyclerView.Adapter<AdapterTask.ViewHolder>() {
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val title_task = itemView.findViewById<TextView>(R.id.title_task)
        val desc_task = itemView.findViewById<TextView>(R.id.desc_task)
        val for_task = itemView.findViewById<TextView>(R.id.for_task)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_task, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.title_task.text = listTask[position].title
        holder.desc_task.text = if (listTask[position].args.despcription != null) listTask[position].args.despcription else ""

        holder.for_task.text = listTask[position].persons_role_2.joinToString(separator = ", "){ person ->  person.firstname}

        holder.itemView.setOnClickListener {
            onClickTask?.onClick(listTask[position])
        }

        holder.itemView.alpha = if (listTask[position].status == -1 || listTask[position].status == 3) 0.5f else 1f
    }

    override fun getItemCount(): Int = listTask.size
}