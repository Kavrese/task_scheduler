package com.example.clonetrello.ProfileScreen

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import com.example.clonetrello.AuthScreen.AuthActivity
import com.example.clonetrello.R
import com.example.clonetrello.clearLocalUserData
import com.example.clonetrello.common.Info
import kotlinx.android.synthetic.main.user_screen.*

class ProfileActivity: AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.user_screen)

        profile_email_txt.text = Info.modelUser!!.email
        profile_name_txt.text = Info.modelUser!!.firstname
        profile_secondname_txt.text = Info.modelUser!!.secondname
        profile_lastname_txt.text = Info.modelUser!!.lastname
        level_user.text = Info.modelUser!!.person_level.toString()

        back_user.setOnClickListener{
            finish()
        }

        exit.setOnClickListener{
            clearLocalUserData(this@ProfileActivity)
            finishAffinity()     //Закрываем все предыдущие экраны ( api < 16 )
            startActivity(Intent(this@ProfileActivity, AuthActivity::class.java))
        }
    }
}