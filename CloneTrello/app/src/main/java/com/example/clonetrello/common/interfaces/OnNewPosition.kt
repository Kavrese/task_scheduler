package com.example.clonetrello.common.interfaces

interface OnNewPosition {
    fun position(pos: Int)
}