package com.example.clonetrello.Adapters

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.widget.NestedScrollView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.clonetrello.*
import com.example.clonetrello.TaskScreen.TaskActivity
import com.example.clonetrello.common.Models.ModelCardDate
import com.example.clonetrello.common.Models.ModelTask
import com.example.clonetrello.common.ToTaskScreen
import com.example.clonetrello.common.interfaces.OnClickTask
import com.example.clonetrello.common.interfaces.OnNewInterface
import com.example.clonetrello.common.interfaces.OnNewPosition
class AdapterDatesSingle(val listDate: List<ModelCardDate>, val onNewInterface: OnNewInterface, val onClickTask: OnClickTask): RecyclerView.Adapter<AdapterDatesSingle.ViewHolder>() {

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val date_card = itemView.findViewById<TextView>(R.id.date_card)
        val days_card = itemView.findViewById<TextView>(R.id.days_card)
        val count_task_card = itemView.findViewById<TextView>(R.id.count_task_card)
        val rec_task_card = itemView.findViewById<RecyclerView>(R.id.rec_task_card)
        val scroll = itemView.findViewById<NestedScrollView>(R.id.scroll)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_date_single_mode, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val count_day = countDays(listDate[position].date)

        val listComplete = listDate[position].list_task.filter { it.status == 3}

        holder.date_card.text = parseData(listDate[position].date)

        val (late, after) = generateWordsForCountDay(count_day.toString())
        holder.days_card.text = "${if (count_day < 0) "${late} " else ""}${Math.abs(count_day)} ${after}"

        holder.count_task_card.text = "Задач: ${listComplete.size}/${listDate[position].list_task.size}"

        val layoutManagerTask = LinearLayoutManager(holder.itemView.context)

        holder.rec_task_card.apply{
            adapter = AdapterTask(listDate[position].list_task, onClickTask)
            layoutManager = layoutManagerTask
        }

        onNewInterface.new_interface(object: OnNewPosition {
            override fun position(pos: Int) {
                holder.scroll.scrollTo(0, 0)
            }
        })


    }

    override fun getItemCount(): Int = listDate.size
}