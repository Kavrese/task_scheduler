package com.example.clonetrello.common.Models

data class ModelLoginBody (
    val email: String,
    val password: String
)
