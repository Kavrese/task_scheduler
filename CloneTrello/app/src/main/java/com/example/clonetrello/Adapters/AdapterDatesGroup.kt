package com.example.clonetrello.Adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.clonetrello.common.Models.ModelCardDate
import com.example.clonetrello.common.interfaces.OnClickDateGroup
import com.example.clonetrello.R
import com.example.clonetrello.countDays
import com.example.clonetrello.generateWordsForCountDay
import com.example.clonetrello.parseData
import java.lang.Math.abs

class AdapterDatesGroup(val listDate: List<ModelCardDate>, val onClickListener: OnClickDateGroup): RecyclerView.Adapter<AdapterDatesGroup.ViewHolder>() {

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val date_group = itemView.findViewById<TextView>(R.id.date_group)
        val days_group = itemView.findViewById<TextView>(R.id.days_group)
        val count_task_group = itemView.findViewById<TextView>(R.id.task_count_group)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_date_group_mode, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val count_day = countDays(listDate[position].date)

        val listComplete = listDate[position].list_task.filter { it.status == 3}

        val (late, after) = generateWordsForCountDay(count_day.toString())
        holder.days_group.text = "${if (count_day < 0) "${late} " else ""}${abs(count_day)} ${after}"

        holder.date_group.text = parseData(listDate[position].date)
        holder.count_task_group.text = "Задач: ${listComplete.size}/${listDate[position].list_task.size}"

        holder.itemView.setOnClickListener {
            onClickListener.onClick(position)
        }
    }

    override fun getItemCount(): Int = listDate.size
}