package com.example.clonetrello.SplashScreen

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.util.Log
import com.example.clonetrello.AuthScreen.AuthActivity
import com.example.clonetrello.R
import com.example.clonetrello.common.ConnectControllerAcceptPoint
import com.example.clonetrello.common.OnGetData

class SplashActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        ConnectControllerAcceptPoint.connectController.connectToLocalHostIfExistElseServer(object:
            OnGetData<String> {
            override fun onGetData(data: String) {
                Log.w("connect-server", "connect to $data")
                startActivity(Intent(this@SplashActivity, AuthActivity::class.java))
                finish()
            }

            override fun onFail(message: String) {
               com.example.clonetrello.showDialog(this@SplashActivity,
                   "Ошибка подключения с серверу", message)
            }
        })
    }
}