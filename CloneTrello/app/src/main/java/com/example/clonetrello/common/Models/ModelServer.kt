package com.example.clonetrello.common.Models

data class ModelServer(
    val main_server: String,
    val backup_server: String,
)
