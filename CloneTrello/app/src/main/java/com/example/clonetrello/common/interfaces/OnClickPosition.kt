package com.example.clonetrello.common.interfaces

import android.view.View

interface OnClickPosition {
    fun onClick(position: Int, view: View)
}