package com.example.clonetrello.CreateEditTaskScreen

import android.app.AlertDialog
import android.app.DatePickerDialog
import android.app.DatePickerDialog.OnDateSetListener
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.PopupMenu
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import com.example.clonetrello.*
import com.example.clonetrello.Adapters.AdapterPerson
import com.example.clonetrello.MainScreen.MainActivity
import com.example.clonetrello.MainScreen.updateDataScreen
import com.example.clonetrello.common.Info
import com.example.clonetrello.common.Models.Args
import com.example.clonetrello.common.Models.ModelPersonFromTask
import com.example.clonetrello.common.Models.ModelTask
import com.example.clonetrello.common.Models.ModelUser
import com.example.clonetrello.common.ToTaskScreen
import com.example.clonetrello.common.interfaces.OnClickPosition
import com.example.clonetrello.common.interfaces.OnGetTask
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.create_task_screen.*
import kotlinx.android.synthetic.main.task_screen.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import kotlin.collections.HashMap

const val ERROR_TITLE = "Ошибка создания задачи"



class CreateEditTaskActivity : AppCompatActivity() {

    object ToActivity{
        lateinit var onGetTask: OnGetTask
    }

    var mapMapMenuItemAndIndexRole2 = hashMapOf<MenuItem, Int>()
    var mapMapMenuItemAndIndexRole1 = hashMapOf<MenuItem, Int>()
    var choosePerson_2: MutableList<ModelPersonFromTask> = arrayListOf()
    var choosePerson_1: MutableList<ModelPersonFromTask> = arrayListOf()
    var allPersons: MutableList<ModelPersonFromTask> = arrayListOf()
    lateinit var onClickPositionRole2: OnClickPosition
    lateinit var onClickPositionRole1: OnClickPosition
    lateinit var onClickDeleteRole2: OnClickPosition
    lateinit var onClickDeleteRole1: OnClickPosition

    var modelTask = if (ToTaskScreen.modelTask != null) ToTaskScreen.modelTask!! else
        ModelTask(
            args = Args(despcription = ""),
            person_id = Info.modelUser!!.person_id,
            status = 0,
            title = "",
            task_id = null,
            ts_bgn = parseDataToString(Date()),
            ts_end = null,
            ts_init = null,
            persons_role_2 = arrayListOf(),
            persons_role_1 = arrayListOf(ModelPersonFromTask(Info.modelUser!!.firstname, Info.modelUser!!.secondname, Info.modelUser!!.lastname, Info.modelUser!!.person_id, 1)),
        )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.create_task_screen)

        getAllPersons()

        onClickPositionRole2 = object: OnClickPosition {
            override fun onClick(position: Int, view: View) {
                showListPersons(view, mapMapMenuItemAndIndexRole2) {
                    val index: Int = mapMapMenuItemAndIndexRole2[it]!!
                    val person = allPersons[index]
                    if (person.person_id in choosePerson_2.map { person -> person.person_id }){
                        Snackbar.make(view, "Данный сотрудник уже добавлен как исполнитель", Snackbar.LENGTH_LONG)
                            .show()
                        return@showListPersons true
                    }
                    (rec_executors.adapter as AdapterPerson).addPersonForAdmin(
                        person
                    )
                    choosePerson_2.add(person)
                    return@showListPersons true
                }
            }
        }

        onClickPositionRole1 = object: OnClickPosition {
            override fun onClick(position: Int, view: View) {
                showListPersons(view, mapMapMenuItemAndIndexRole1) {
                    val index: Int = mapMapMenuItemAndIndexRole1[it]!!
                    val person = allPersons[index]
                    if (person.person_id in choosePerson_1.map { person -> person.person_id }){
                        Snackbar.make(view, "Данный сотрудник уже добавлен как руководитель", Snackbar.LENGTH_LONG)
                            .show()
                        return@showListPersons true
                    }
                    (rec_bosses.adapter as AdapterPerson).addPersonForAdmin(
                        person
                    )
                    choosePerson_1.add(person)
                    return@showListPersons true
                }
            }
        }

        onClickDeleteRole2 = object: OnClickPosition {
            override fun onClick(position: Int, view: View) {
                AlertDialog.Builder(this@CreateEditTaskActivity, R.style.AlertDialogTheme)
                    .setTitle("Удаление исполнителя")
                    .setMessage("Вы уверены, что хотите удалить исполнителя ${(view as TextView).text} из задачи?")
                    .setPositiveButton("Да"){_, _ ->
                        removeExecutor(position)
                    }
                    .setNegativeButton("Нет", null)
                    .show()
            }
        }

        onClickDeleteRole1 = object: OnClickPosition {
            override fun onClick(position: Int, view: View) {
                if (choosePerson_1[position].person_id != Info.modelUser!!.person_id)
                    AlertDialog.Builder(this@CreateEditTaskActivity, R.style.AlertDialogTheme)
                        .setTitle("Удаление руководителя")
                        .setMessage("Вы уверены, что хотите удалить руководителя ${(view as TextView).text} из задачи?")
                        .setPositiveButton("Да"){_, _ ->
                            removeBoss(position)
                        }
                        .setNegativeButton("Нет", null)
                        .show()
                else
                    AlertDialog.Builder(this@CreateEditTaskActivity, R.style.AlertDialogTheme)
                        .setTitle("Ошибка удаления руководителя")
                        .setMessage("Вы не можете удалить себя из руководителей задачи")
                        .setNegativeButton("OK", null)
                        .show()
            }
        }

        save_admin_create_task.setOnClickListener {
            it.isEnabled=false
            saveTask()
        }

        deadline_task_cts.setOnClickListener {
            chooseDate { _, year, month, dayOfMonth ->
                val c = Calendar.getInstance()
                c.set(Calendar.YEAR, year)
                c.set(Calendar.DAY_OF_MONTH, dayOfMonth)
                val date = c.time
                //val str_date = parseDataToString(date)
                deadline_task_cts.text = toShortDateFormat(date)
            }
        }

        setDataToView(modelTask)
    }

    private fun saveTask() {
        val title = title_task_cts.text.toString()
        val desc = description_task_cts.text.toString()
        val end =
            if (deadline_task_cts.text.toString() != "Дата") toLongFromShortString(deadline_task_cts.text.toString()) else ""

        //val date_date_end = parseDataFromString(end)

        if (title.isEmpty() || end.isEmpty()) {
            showDialog(this, ERROR_TITLE, "Заполните все необходимые поля")
            save_admin_create_task.isEnabled=true
            return
        }

        if (choosePerson_1.isEmpty()) {
            showDialog(this, ERROR_TITLE, "Выберите руководителей для задачи")
            save_admin_create_task.isEnabled=true
            return
        }

        if (choosePerson_2.isEmpty()) {
            showDialog(this, ERROR_TITLE, "Выберите исполнителей для задачи")
            save_admin_create_task.isEnabled=true
            return
        }

        if (title.count() >= 256) {
            showDialog(this, "Ошибка сохранения", "Максимум символов для заголовка 128")
            save_admin_create_task.isEnabled=true
            return
        }

        if (end == "") {
            showDialog(this, "Ошибка сохранения", "Дата окончания задания не выбрана")
            save_admin_create_task.isEnabled=true
            return
        }
        /*
        if (modelTask.ts_end < modelTask.ts_bgn) {
            return
        }
        */
        if (modelTask.task_id == null) {
            val modelTask = ModelTask(
                args = Args(despcription = if (desc.isNotEmpty()) desc else null),
                person_id = Info.modelUser!!.person_id,
                status = 0,
                title = title,
                task_id = null,
                ts_bgn = parseDataToString(Date()),
                ts_end = end,
                ts_init = null,
                persons_role_2 = choosePerson_2,
                persons_role_1 = choosePerson_1
            )
            createTask(modelTask, save_admin_create_task)
            ToActivity.onGetTask.onGood(modelTask)
        }else{
            modelTask.args = Args(despcription = if (desc.isNotEmpty()) desc else null)
            modelTask.persons_role_1 = choosePerson_1
            modelTask.persons_role_2 = choosePerson_2
            modelTask.ts_end = end
            modelTask.title = title
            updateTask(modelTask)
        }
    }

    private fun initRecExecutors(listExecutors: List<ModelPersonFromTask>, onClickPosition: OnClickPosition, onClickDelete: OnClickPosition ) {
        val spanCount = 2
        rec_executors.apply {
            adapter = AdapterPerson(listExecutors.toMutableList(), true, onClickPosition, onClickDelete, R.layout.item_add_executor)
            layoutManager = GridLayoutManager(
                this@CreateEditTaskActivity,
                spanCount,
                GridLayoutManager.HORIZONTAL,
                false
            )
        }
    }

    private fun initRecBosses(listBosses: List<ModelPersonFromTask>, onClickPosition: OnClickPosition, onClickDelete: OnClickPosition ) {
        val spanCount = 2
        rec_bosses.apply {
            adapter = AdapterPerson(listBosses.toMutableList(), true, onClickPosition, onClickDelete, R.layout.item_add_boss)
            layoutManager = GridLayoutManager(
                this@CreateEditTaskActivity,
                spanCount,
                GridLayoutManager.HORIZONTAL,
                false
            )
        }
    }

    private fun removeExecutor(position: Int) {
        choosePerson_2.removeAt(position)
        (rec_executors.adapter as AdapterPerson).removePersonForAdmin(position)
    }

    private fun removeBoss(position: Int) {
        choosePerson_1.removeAt(position)
        (rec_bosses.adapter as AdapterPerson).removePersonForAdmin(position)
    }

    private fun setDataToView(modelTask: ModelTask) {
        choosePerson_1 = modelTask.persons_role_1.toMutableList()
        choosePerson_2 = modelTask.persons_role_2.toMutableList()
        title_task_cts.setText(modelTask.title)
        if (modelTask.ts_end != null) deadline_task_cts.text = toShortDateFormat(modelTask.ts_end!!)
        status_create_task.text = parseIntToStringStatus(modelTask.status)
        if (modelTask.args.despcription != null)
            description_task_cts.setText(modelTask.args.despcription)

        if (modelTask.task_id != null) {
            choosePerson_2.clear()
            choosePerson_1.clear()
            choosePerson_2.addAll(modelTask.persons_role_2)
            choosePerson_1.addAll(modelTask.persons_role_1)
            initRecExecutors(modelTask.persons_role_2, onClickPositionRole2, onClickDeleteRole2)
            initRecBosses(modelTask.persons_role_1, onClickPositionRole1, onClickDeleteRole1)
        }else {
            initRecExecutors(choosePerson_2, onClickPositionRole2, onClickDeleteRole2)
            initRecBosses(choosePerson_1, onClickPositionRole1, onClickDeleteRole1)
        }


    }

    private fun chooseDate(onDateSelected: OnDateSetListener) {
        val c = Calendar.getInstance()
        val mYear = c.get(Calendar.YEAR)
        val mMonth = c.get(Calendar.MONTH)
        val mDay = c.get(Calendar.DAY_OF_MONTH)

        val datePickerDialog = DatePickerDialog(this, onDateSelected, mYear, mMonth, mDay)
        datePickerDialog.show()
    }

    private fun createTask(modelTask: ModelTask, view: View) {
        initRetrofit().createTask(body = modelTask).enqueue(object : Callback<ModelTask> {
            override fun onResponse(
                call: Call<ModelTask>,
                response: Response<ModelTask>
            ) {
                if (response.body() != null) {
                    Snackbar.make(view, "Задание создано", Snackbar.LENGTH_LONG).show()
                    val intent = Intent(baseContext, MainActivity::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
                    startActivity(intent)

                } else {
                    save_admin_create_task.isEnabled=true
                    showDialog(this@CreateEditTaskActivity, ERROR_TITLE)
                }
            }

            override fun onFailure(call: Call<ModelTask>, t: Throwable) {
                save_admin_create_task.isEnabled=true
                showDialog(this@CreateEditTaskActivity, ERROR_TITLE, t)
            }
        })
    }

    private fun updateTask(modelTask: ModelTask){
        initRetrofit().updateTask(body = modelTask).enqueue(object: Callback<ModelTask>{
            override fun onResponse(call: Call<ModelTask>, response: Response<ModelTask>) {
                if (response.isSuccessful && response.body() != null){
                    ToActivity.onGetTask.onGood(response.body()!!.updateStatus())
                    Snackbar.make(save_admin_create_task, "Задача сохранена", Snackbar.LENGTH_LONG).show()
                    updateDataScreen.onClick(null)
                    finish()
                }else{
                    save_admin_create_task.isEnabled=true
                    showDialog(this@CreateEditTaskActivity, "Ошибка сохранения задачи")
                }
            }

            override fun onFailure(call: Call<ModelTask>, t: Throwable) {
                save_admin_create_task.isEnabled=true
                showDialog(this@CreateEditTaskActivity, "Ошибка сохранения задачи", t)
            }
        })
    }

    private fun showListPersons(view: View,  mapMapMenu: HashMap<MenuItem, Int>, onMenuItemClickListener: PopupMenu.OnMenuItemClickListener) {
        val popupMenu = PopupMenu(this@CreateEditTaskActivity, view)
        val menu = popupMenu.menu!!
        allPersons.forEachIndexed { index, person -> menu.add("${person.secondname} ${person.firstname} ${person.lastname}"); mapMapMenu[menu.getItem(menu.size() - 1) as MenuItem] = index}
        popupMenu.setOnMenuItemClickListener(onMenuItemClickListener)
        popupMenu.show()
    }

    private fun getAllPersons(){
        initRetrofit().getAllPerson().enqueue(object : Callback<List<ModelUser>> {
            override fun onResponse(
                call: Call<List<ModelUser>>,
                response: Response<List<ModelUser>>
            ) {
                if (response.body() != null) {
                    allPersons = response.body()!!.map{
                        ModelPersonFromTask(it.firstname, it.secondname, it.lastname, it.person_id, -1)}.toMutableList()
                } else {
                    showDialog(this@CreateEditTaskActivity, "Ошибка получения списка пользователей")
                }
            }

            override fun onFailure(call: Call<List<ModelUser>>, t: Throwable) {
                showDialog(this@CreateEditTaskActivity, "Ошибка получения списка пользователей", t)
            }
        })
    }

    override fun onBackPressed() {
        AlertDialog.Builder(this, R.style.AlertDialogTheme)
            .setTitle("Отмена изменений")
            .setMessage("Сохранить изменения?")
            .setPositiveButton("Да") { p0, p1 ->
                saveTask()
                finish()
            }
            .setNegativeButton("Нет"){ p0, p1 ->
                finish()
            }
            .show()
    }
}