package com.example.clonetrello.TaskScreen

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.PopupMenu
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import com.example.clonetrello.*
import com.example.clonetrello.Adapters.AdapterPerson
import com.example.clonetrello.CreateEditTaskScreen.CreateEditTaskActivity
import com.example.clonetrello.common.Models.ModelPersonFromTask
import com.example.clonetrello.common.Models.ModelTask
import com.example.clonetrello.common.ToTaskScreen
import com.example.clonetrello.common.interfaces.OnGetTask
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.create_task_screen.*
import kotlinx.android.synthetic.main.task_screen.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class TaskActivity : AppCompatActivity() {
    var modelTask = ToTaskScreen.modelTask
    lateinit var dialogSendCompleteTask: AlertDialog
    var isAdmin = false
    private val onGetToPrepare = object: OnGetTask{
        override fun onGood(modelTask: ModelTask) {
            prepareModelTask(modelTask)
        }
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.task_screen)
        val taskId = intent.extras!!.getInt("taskId")
        getTask(taskId, onGetToPrepare)
    }

    private fun prepareModelTask(modelTask: ModelTask){
        this@TaskActivity.modelTask = modelTask
        val personId = getSharedPreferences("0", 0).getString("person_id", null)
        if (personId != null && personId.toInt() in modelTask.persons_role_1.map { it.person_id })
            onAdminVersion()
        else
            onWorkerVersion()
        setDataToView(this@TaskActivity.modelTask!!)
    }

    private fun getTask(taskId: Int, onGetTask: OnGetTask){
        initRetrofit().taskId(taskId=taskId).enqueue(object: Callback<ModelTask>{
            override fun onResponse(call: Call<ModelTask>, response: Response<ModelTask>) {
                if (response.body() != null && response.isSuccessful){
                    onGetTask.onGood(response.body()!!.updateStatus())
                }else{
                    showDialog(this@TaskActivity, "Ошибка получения задачи")
                }
            }

            override fun onFailure(call: Call<ModelTask>, t: Throwable) {
                showDialog(this@TaskActivity, "Ошибка получения задачи", t)
            }
        })
    }

    private fun onAdminVersion(){
        isAdmin = true
        complete_worker.visibility = View.GONE
        edit_admin_task.visibility = View.VISIBLE
        rec_files_message.visibility = View.GONE
        card_message.visibility = View.GONE

        edit_admin_task.setOnClickListener {
            CreateEditTaskActivity.ToActivity.onGetTask = onGetToPrepare
            startActivity(Intent(this, CreateEditTaskActivity::class.java))
        }

        val popupMenu = PopupMenu(this, status_task)
        if (modelTask!!.status == -1 || modelTask!!.status == 3)
            popupMenu.menu.add("Возобновить задачу")
        else
            popupMenu.menu.add("Отменить задачу")
        if (modelTask!!.status != 3)
            if (modelTask!!.status == 2)
                popupMenu.menu.add("Подтвердить выполнение задачи")
            else
                popupMenu.menu.add("Выполнить задачу")
        popupMenu.setOnMenuItemClickListener {
            val title = it.title
            AlertDialog.Builder(this, R.style.AlertDialogTheme)
                .setTitle("Изменение статуса задачи")
                .setMessage("Вы уверены, что хотите $title?")
                .setNegativeButton("Нет", null)
                .setPositiveButton("Да") {_,_ ->
                    initRetrofit().newStatus(idTask = modelTask!!.task_id.toString(), newStatus = when(title) {
                        "Отменить задачу" -> -1
                        "Возобновить задачу" -> 0
                        "Подтвердить выполнение задачи", "Выполнить задачу" -> 3
                        else -> modelTask!!.status
                    }).enqueue(object: Callback<ModelTask>{
                        override fun onResponse(
                            call: Call<ModelTask>,
                            response: Response<ModelTask>
                        ) {
                            if (response.isSuccessful && response.body() != null){
                                onGetToPrepare.onGood(response.body()!!)
                            }else{
                                showDialog(this@TaskActivity, "Ошибка изменения статуса задачи")
                            }
                        }

                        override fun onFailure(call: Call<ModelTask>, t: Throwable) {
                            showDialog(this@TaskActivity, "Ошибка изменения статуса задачи", t)
                        }

                    })
                }.show()
            return@setOnMenuItemClickListener true
        }
        status_task.setOnClickListener {
            popupMenu.show()
        }
    }

    private fun onWorkerVersion(){
        isAdmin = false
        prepareDialogAlert()

        if (modelTask!!.status in arrayOf(3, 2, -1)) {
            complete_worker.visibility = View.GONE
            card_message.visibility = View.GONE
        } else {
            complete_worker.visibility = View.VISIBLE
            card_message.visibility = View.VISIBLE
        }
        edit_admin_task.visibility = View.GONE
        rec_files_message.visibility = View.VISIBLE


        complete_worker.setOnClickListener {
            it.isEnabled = false
            dialogSendCompleteTask.show()
        }
    }

    private fun prepareDialogAlert(){
        dialogSendCompleteTask = AlertDialog.Builder(this, R.style.AlertDialogTheme)
            .setTitle("Задание готово")
            .setMessage("Вы уверены, что задание готово?")
            .setNegativeButton("Нет") { _, _ -> complete_worker.isEnabled = true }
            .setPositiveButton("Да"){_, _ ->
                sendComplete(modelTask!!)
            }
            .create()
    }

    private fun setDataToView(modelTask: ModelTask){
        title_task.text = modelTask.title
        deadline_task.text = toShortDateFormat(modelTask.ts_end!!)
        status_task.text = parseIntToStringStatus(modelTask.status)
        if (modelTask.args.despcription != null)
            description_task.text = modelTask.args.despcription
        initRecBoss(modelTask.persons_role_1)
        initRecExecutors(modelTask.persons_role_2)
    }

    private fun initRecExecutors(listExecutors: List<ModelPersonFromTask>){
        val spanCount = 2
        rec_executors_task.apply {
            adapter = AdapterPerson(listExecutors.toMutableList(), false)
            layoutManager = GridLayoutManager(this@TaskActivity, spanCount, GridLayoutManager.HORIZONTAL, false)
        }
    }

    private fun initRecBoss(listBoss: List<ModelPersonFromTask>){
        val spanCount = 2
        rec_boss_task.apply {
            adapter = AdapterPerson(listBoss.toMutableList(), false)
            layoutManager = GridLayoutManager(this@TaskActivity, spanCount, GridLayoutManager.HORIZONTAL, false)
        }
    }

    private fun sendComplete(modelTask: ModelTask){
        initRetrofit().newStatus(idTask = modelTask.task_id.toString()).enqueue(object: Callback<ModelTask>{
            override fun onResponse(call: Call<ModelTask>, response: Response<ModelTask>) {
                if (response.body() != null){
                    this@TaskActivity.modelTask = response.body()!!
                    setDataToView(modelTask)
                    finish()
                }else{
                    showDialog(this@TaskActivity, "Ошибка изменение статуса")
                }
            }

            override fun onFailure(call: Call<ModelTask>, t: Throwable) {
                showDialog(this@TaskActivity, "Ошибка изменение статуса", t)
            }

        })
    }

    fun onReturnClicked(v: View) {
        finish()
    }
}
