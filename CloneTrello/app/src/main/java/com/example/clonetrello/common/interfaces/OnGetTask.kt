package com.example.clonetrello.common.interfaces

import com.example.clonetrello.common.Models.ModelTask

interface OnGetTask {
    fun onGood(modelTask: ModelTask)
}
