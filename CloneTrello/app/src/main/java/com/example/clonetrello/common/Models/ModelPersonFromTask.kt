package com.example.clonetrello.common.Models

data class ModelPersonFromTask(
    var firstname: String,
    var secondname: String,
    var lastname: String,
    var person_id: Int,
    var role_id: Int
)