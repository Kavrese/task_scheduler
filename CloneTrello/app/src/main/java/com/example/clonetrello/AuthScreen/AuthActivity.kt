package com.example.clonetrello.AuthScreen

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.example.clonetrello.MainScreen.MainActivity
import com.example.clonetrello.R
import com.example.clonetrello.common.Info
import com.example.clonetrello.common.Models.ModelUser
import com.example.clonetrello.*
import com.example.clonetrello.common.Models.ModelLoginBody
import com.example.clonetrello.common.interfaces.OnCompleteAuth
import com.example.clonetrello.initRetrofit
import com.google.firebase.ktx.Firebase
import com.google.firebase.messaging.FirebaseMessaging
import kotlinx.android.synthetic.main.activity_auth.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AuthActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_auth)

        //При прохождении авторизации
        val onCompleteAuth = object: OnCompleteAuth {
            override fun onAuth(modelUser: ModelUser, password: String) {
                FirebaseMessaging.getInstance().sendTokenToAPI(modelUser.token)
                saveLocalInfoUser(modelUser, password)
                Info.modelUser = modelUser
                startActivity(Intent(this@AuthActivity, MainActivity::class.java))
                finish()
            }
        }

        if (checkIsAuthAlready()){
            val lastData = loadLocalUserInfo()
            email.setText(lastData[0])
            password.setText(lastData[1])
            auth(lastData[0], lastData[1], onCompleteAuth)
        }

        card_auth.setOnClickListener {
            val email = email.text.toString()
            val password = password.text.toString()
            prepareAuth(email, password, onCompleteAuth)
        }
    }

    //Метод авторизации с проверкой полей
    private fun prepareAuth(email: String, password: String, onCompleteAuth: OnCompleteAuth){
        if (email.isNotEmpty() && password.isNotEmpty()) {
            if (email.count() < 128 && password.count() < 128) {
                auth(email, password, onCompleteAuth)
            }else{
                showDialog(this, "Ошибка авторизации", "Максимум символов для почты 128\nМаксимум символов для пароля 128")
            }
        }else{
            showDialog(this, "Ошибка авторизации", "Необходимо заполнить все поля")
        }
    }

    //Метод проверки наличия пользовательских данных в памяти
    private fun checkIsAuthAlready(): Boolean{
        val lastData = loadLocalUserInfo()
        return lastData[0].isNotEmpty() && lastData[1].isNotEmpty()
    }

    //Метод сохранения пользовательских данных в память
    private fun saveLocalInfoUser(modelUser: ModelUser, password: String){
        val sh = getSharedPreferences("0", 0)
        sh.edit()
            .putString("email", modelUser.email)
            .putString("password", password)
            .putString("person_id", modelUser.person_id.toString())
            .apply()
    }

    //Метод загрузки пользовательских данных из памяти
    private fun loadLocalUserInfo(): ArrayList<String>{
        val sh = getSharedPreferences("0", 0)
        return arrayListOf(sh.getString("email", "")!!, sh.getString("password", "")!!)
    }

    //Метод авторизации
    private fun auth(email: String, password: String, onCompleteAuth: OnCompleteAuth){
        showAuthWindow()
        initRetrofit().login(ModelLoginBody(email, password)).enqueue(object : Callback<ModelUser> {
            override fun onResponse(
                call: Call<ModelUser>,
                response: Response<ModelUser>
            ) {
                if (response.body() != null) {
                    onCompleteAuth.onAuth(response.body()!!, password)
                } else {
                    showDialog(this@AuthActivity, "Ошибка авторизации: ${response.code()}")
                    hideAuthWindow()
                }
            }

            override fun onFailure(call: Call<ModelUser>, t: Throwable) {
                showDialog(
                    this@AuthActivity,
                    "Ошибка авторизации",
                    t
                )
                hideAuthWindow()
            }
        })
    }

    private fun showAuthWindow(){
        window_auth.visibility = View.VISIBLE
    }

    private fun hideAuthWindow(){
        window_auth.visibility = View.GONE
    }
}