package com.example.clonetrello.common

import com.example.clonetrello.common.Models.ModelPersonFromTask

interface OnGetExecutors {
    fun getExecutors(parse: List<ModelPersonFromTask>)
}
