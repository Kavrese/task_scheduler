package com.example.clonetrello.common.Models

import com.example.clonetrello.countDays

class ModelTask(
    var args: Args,
    var person_id: Int,
    var status: Int,
    var task_id: Int?,
    var title: String,
    var ts_bgn: String,
    var ts_end: String?,
    var ts_init: String?,
    var persons_role_2: List<ModelPersonFromTask>,
    var persons_role_1: List<ModelPersonFromTask>,
){
    fun updateStatus(): ModelTask{
        if (status == 0 && ts_end != null && countDays(ts_end!!) < 0){
            status = -2     //Задача просреченна
        }
        return this
    }
}

data class Args(
    var despcription: String?
)