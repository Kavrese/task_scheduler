package com.example.clonetrello.Adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.clonetrello.R
import com.example.clonetrello.common.Models.ModelPersonFromTask
import com.example.clonetrello.common.interfaces.OnClickPosition

class AdapterPerson(private val persons: MutableList<ModelPersonFromTask>,
                    private val showAddButton: Boolean,
                    private val onClickPosition: OnClickPosition? = null,
                    private val onClickDelete: OnClickPosition? = null,
                    private val idImgAddButton: Int? = null
): RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    class PersonViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val fio_person = itemView.findViewById<TextView>(R.id.fio_person)
        val delete_person = itemView.findViewById<ImageView>(R.id.delete_person)
    }

    class AddExecutorViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val button = itemView.findViewById<ImageView>(R.id.add)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == 0) PersonViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_executor, parent, false))
        else AddExecutorViewHolder(LayoutInflater.from(parent.context).inflate(idImgAddButton!!, parent, false))
    }

    override fun onBindViewHolder(holderExecutor: RecyclerView.ViewHolder, position: Int) {
        if (getItemViewType(position) == 0){
            holderExecutor as PersonViewHolder
            holderExecutor.fio_person.text = persons[position].firstname

            if (onClickDelete != null)
                holderExecutor.delete_person.setOnClickListener {
                    onClickDelete.onClick(position, holderExecutor.fio_person)
                }
            else
                holderExecutor.delete_person.visibility = View.GONE

        }else{
            holderExecutor as AddExecutorViewHolder
            holderExecutor.button.setOnClickListener{
                onClickPosition!!.onClick(position, holderExecutor.itemView)
            }
        }
    }

    override fun getItemCount(): Int  = if (showAddButton) persons.size + 1 else persons.size

    override fun getItemViewType(position: Int): Int {
        return if (position == persons.size && showAddButton) 1 else 0
    }

    fun addPersonForAdmin(person: ModelPersonFromTask){
        persons.add(person)
        notifyItemInserted(persons.size)
    }

    fun removePersonForAdmin(position: Int){
        persons.removeAt(position)
        notifyItemRemoved(position)
    }
}