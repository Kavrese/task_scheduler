package com.example.clonetrello

import org.junit.Test

import org.junit.Assert.*

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {
    @Test
    fun text_parse_data(){
        assertEquals("09/10/2021", parseData("Sat, 09 Oct 2021 00:00:00"))
    }
}