﻿--Добавить данные в таблицу event:
INSERT INTO "work".event (ts,details)
	VALUES ('10-04-2021','json_file');

--Выбрать детали задания по id:
select details from "work".event where task_id = 2;

--Обновить timestamp событию по id:
update "work".event set ts = '11-04-2021' where event_id = 1;

--Удалить запись c id=1:
delete from "work".event where event_id = 1