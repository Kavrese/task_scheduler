﻿--Добавить новую запись:
INSERT INTO "work".person ("token",email,"password",firstname,secondname,lastname,"position")
	VALUES ('5764','test@mail.ru','1234','Name','Sname','Lname','2');

--Узнать ФИО по id:
select firstname, lastname, secondname from "work".person where person_id = 1;

--Узнать id по имени:
select person_id from "work".person where firstname = 'Ivan';

--Изменить имя и фамилию по id:
update "work".person set firstname = 'Petya', lastname = 'Sergeevich' where person_id = 1;

--Изменить токен по id:
update "work".person set token = ... where person_id = 1;

--Изменить пароль по id:
update "work".person set password = 'new_pass' where person_id = 1;

--Изменить позицию по id:
update "work".person set position = 'new_pos' where person_id = 1;

--Удалить человека c id=1:
delete from "work".person where person_id = 1