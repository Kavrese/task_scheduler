﻿--Добавить новое задание:
INSERT INTO "work".task (ts_bgn,ts_end,ts_init,person_id ,title ,args ,status)
	VALUES ('10-04-2021','15-04-2021','9-04-2021',1,'Title','json_file','1');

--Выбрать заголовок, аргументы и статус задания по id:
select title, args, status from "work".task where task_id = 1;

--Выбрать заголовок, аргументы и статус заданий человека по id:
select title, args, status from "work".task where person_id = 1;

--Изменить статус задания по task_id:
update "work".task set status = '2' where task_id = 1;

--Удалить задание c id=1:
delete from "work".task where task_id = 1