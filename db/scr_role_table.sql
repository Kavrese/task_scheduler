﻿--Добавить новую роль:
INSERT INTO "work"."role" (name,description,"level")
	VALUES ('Name_role','desc_role',1);

--Выбрать описание роли по id:
select description from "work"."role" where role_id = 1;

--Изменить level роли по id:
update "work"."role" set level = 2 where role_id = 1;

--Удалить роль c id=1:
delete from "work"."role" where role_id = 1