CREATE SCHEMA "task_scheduler" AUTHORIZATION postgres;

CREATE TABLE "task_scheduler".person (
	person_id int4 NOT NULL GENERATED ALWAYS AS IDENTITY,
	"token" varchar(32) Unique,
	phone varchar(32) NOT NULL Unique,
	email varchar(128) NOT NULL Unique,
	"password" varchar(128) NOT NULL,
	firstname varchar(128) NOT NULL,
	secondname varchar(128),
	lastname varchar(128) NOT NULL,
	"position" varchar(128),
	CONSTRAINT task_scheduler_person_pk PRIMARY KEY (person_id)
);

CREATE TABLE "task_scheduler"."role" (
	role_id int4 NOT NULL GENERATED ALWAYS AS IDENTITY,
	"name" varchar(256) NOT NULL,
	description varchar(2048) NOT NULL,
	"level" int4 NOT null,
	CONSTRAINT task_scheduler_role_pk PRIMARY KEY (event_id)
);

CREATE TABLE "task_scheduler".task (
	task_id int4 NOT NULL GENERATED ALWAYS AS IDENTITY,
	ts_bgn timestamp(6) without time zone NOT NULL,
	ts_end timestamp(6) without time zone NOT NULL,
	ts_init timestamp(6) without time zone NOT NULL,
	person_id int4 NOT NULL,
	title varchar(256) NOT NULL,
	args json NOT NULL,
	status int4 NOT null,
	CONSTRAINT task_scheduler_task_pk PRIMARY KEY (task_id)
);

CREATE TABLE "task_scheduler".person_task (
	task_id int4 NOT NULL,
	person_id int4 NOT NULL,
	role_id int4 NOT null,
	CONSTRAINT task_scheduler_person_task_pk PRIMARY KEY (task_id, person_id, role_id)
);

CREATE TABLE "task_scheduler"."event" (
	event_id int4 NOT NULL GENERATED ALWAYS AS IDENTITY,
	task_id int4 NOT NULL,
	person_id int4 NOT NULL,
	ts timestamp(6) without time zone NOT NULL,
	details json NOT NULL,
	CONSTRAINT task_scheduler_event_pk PRIMARY KEY (event_id)
);
