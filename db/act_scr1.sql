--Получить инфу о задании по id
select title, description, ts_bgn, ts_end from task_scheduler.task where task_id = idTask
--Получить пользователя по токену
select firstname, secondname, lastname, email from task_scheduler.person where token = token
--Изменение статуса задачи
update task_scheduler.task set status = newStatus where task_id = idTask;
insert into task_scheduler."event" (task_id,person_id,ts,details) 
values (idTask,idPerson,ts_,"Событие изменения статуса");
select * from task_scheduler.task where task_id = idTask;
--Получение всех задач в определённом промежутке времени
select * from task_scheduler.task where ts_end between startTask and endTask;
--Авторизация пользователя 
update task_scheduler.person set token = newToken where email = email and "password" = "password" ;
select * from task_scheduler.person where "token" = newToken;
--Поиск person
select * from task_scheduler.person where "secondname" LIKE ('text_search%')or "firstname" LIKE ('text_search%') or "lastname" LIKE ('text_search%');
--Поиск task
select * from task_scheduler.task  where title LIKE ('text_search%');
--Список людей задачи по id с ролями
select p.person_id, p.firstname, pt.role_id
from task_scheduler.person as p
inner join task_scheduler.person_task as pt
on p.person_id = pt.person_id
inner join task_scheduler.task as t
on pt.task_id = t.task_id
where t.task_id = idTask
/**Проверка на авторизацию
select firstname, secondname, lastname
case token
when is not null then (token as int4(32))
when null        then 'Пользователь не авторизирован' 
end**/
--Удаление задачи
delete from task_scheduler.task where task_id = idTask;
delete from task_scheduler.person_task where task_id = idTask;
delete from task_scheduler."event" where task_id = idTask;
--Редактирование задачи
update task_scheduler.task set 'колонка_таблицы' = значение where task_id = idTask;
--Создание ивента редактирования задачи
insert into task_scheduler."event" (task_id,person_id,ts,details) 
values (idTask,idPerson,ts_,details);
--Создание event
insert into task_scheduler."event" (task_id,person_id,ts,details) 
values (1,2,'2021-09-11 00:00:00.000','{"description": "TestEvent"}');
--Создание задания
insert into task_scheduler.task (ts_bgn,ts_end,ts_init,person_id,title,args,status) 
values ('2021-09-11 00:00:00.000','2021-09-11 00:00:00.000','2021-09-11 00:00:00.000',1,"Заголовок1",'{"description": "task description"}',0);
insert into task_scheduler."event" (task_id,person_id,ts,details) 
values (1,1,2021-09-11 00:00:00.000,'{"description": "Задание создано"}');
insert into task_scheduler.person_task (task_id,person_id,role_id) 
values (1,1,2);
--Создание роли
insert into task_scheduler."role" (name,description,task_level) 
values (name_role,desc_role,task_level_role);